NeoLemmix Pack Toolkit

by Stephan Neupert

This is an application to manage level packs for NeoLemmix. It combines all the
single levels, distributes them among ranks, sets up custom sprites e.g. for
rank images or the player background, adds music and talismans and ensures that
everything can be correctly read by the NeoLemmix player.

Compile informations:
- Build "PackToolkit.sln" (e.g. with Visual Studio 2015 or newer)
- The Pack Toolkit requires C# 6.0 (or newer)
- There are no dependencies on external libraries.

To build on Linux, install Mono and run xbuild without any arguments. 
Note that the Pack Toolkit was mainly written for Windows (as the NeoLemmix player is 
Windows-only), so graphical issues may be expected on Linux.

Any bugs, feature requests and support issues are currently handled via the lemmings forums at
  www.lemmingsforums.net
My nickname there is Nepster.  

Copyright: CC BY-NC 4.0 (2017)