﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace PackToolkit
{
  public partial class FormMain : Form
  {
    public FormMain()
    {
      InitializeComponent();

      ResultTextBoxes = new Dictionary<C.ResultTextType, MultilineTextBox>()
            {
                { C.ResultTextType.All, cmbResultText1 }, { C.ResultTextType.Great, cmbResultText2 },
                { C.ResultTextType.Good, cmbResultText3 }, { C.ResultTextType.Exact, cmbResultText4 },
                { C.ResultTextType.OneShort, cmbResultText5 }, { C.ResultTextType.NearPass, cmbResultText6 },
                { C.ResultTextType.Bad, cmbResultText7 }, { C.ResultTextType.VeryBad, cmbResultText8 },
                { C.ResultTextType.None, cmbResultText9 }
            };

      curPack = new Pack();
      isSaved = false;
      WritePackInfoToForm();
    }

    Pack curPack;
    string packDirectory;
    bool isSaved;

    private Dictionary<C.ResultTextType, MultilineTextBox> ResultTextBoxes;

    private void WritePackInfoToForm()
    {
      txtTitle.Text = curPack.Title;
      txtTitle2.Text = curPack.Title2;

      cmbScrollerText.Text = curPack.ScrollerText[0];
      cmbScrollerText.Items.Clear();
      foreach (string scrolltext in curPack.ScrollerText)
      {
        cmbScrollerText.Items.Add(scrolltext);
      }

      foreach (C.ResultTextType resultTextType in C.ResultTextTypeArray)
      {
        MultilineTextBox textBox = ResultTextBoxes[resultTextType];
        textBox.Text = curPack.ResultText[resultTextType][0];
        textBox.Items.Clear();
        foreach (string text in curPack.ResultText[resultTextType])
        {
          textBox.Items.Add(text);
        }
      }

      WriteRankInfo();
      WriteLevelInfo();
      WriteTalismanInfo();
    }

    /// <summary>
    /// Sets all items in the listRank correctly
    /// </summary>
    private void WriteRankInfo()
    {
      listRanks.Items.Clear();

      if (curPack.Ranks.Count == 0)
      {
        listRanks.Items.Add("No ranks...");
      }
      else
      {
        foreach (Rank rank in curPack.Ranks)
        {
          listRanks.Items.Add(rank.Name);
        }
      }
    }

    /// <summary>
    /// Sets all items in the listLevel correctly
    /// </summary>
    private void WriteLevelInfo()
    {
      listLevels.Items.Clear();

      if (GetCurRank() == null || GetCurRank().Levels.Count == 0)
      {
        listLevels.Items.Add("No levels...");
      }
      else
      {
        foreach (Level level in GetCurRank().Levels)
        {
          listLevels.Items.Add(level.Name);
        }
      }
    }

    /// <summary>
    /// Sets all items in the ListTalisman correctly
    /// </summary>
    public void WriteTalismanInfo()
    {
      listTalismans.Items.Clear();

      if (GetCurLevel() == null || GetCurLevel().Talismans.Count == 0)
      {
        listTalismans.Items.Add("No talismans...");
      }
      else
      {
        foreach (Talisman talisman in GetCurLevel().Talismans)
        {
          string text = "Talisman " + (GetCurLevel().Talismans.IndexOf(talisman) + 1).ToString()
                                    + " (" + talisman.AwardType.ToString() + ")";
          listTalismans.Items.Add(text);
        }
      }
    }

    private void exitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (!isSaved)
      {
        DialogResult dialogResult = MessageBox.Show("Do you want to save this pack?", "Save pack?", MessageBoxButtons.YesNo);
        if (dialogResult == DialogResult.Yes)
        {
          saveToolStripMenuItem_Click(null, null);
        }
      }

      Application.Exit();
    }

    /// <summary>
    /// Saves a pack to a user-specified directory.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void saveToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        isSaved = SavePack.Save(curPack, packDirectory);
        if (isSaved)
        {
          MessageBox.Show("Level pack is successfully saved", "Success");
        }
      }
      catch (Exception Ex) when (!Env.Debugging)
      {
        Utility.LogException(Ex);
        MessageBox.Show("Could not save the pack: " + Ex.Message, "Error saving the pack");
      }
    }

    /// <summary>
    /// Loads a pack from a user-specified directory.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void loadToolStripMenuItem_Click(object sender, EventArgs e)
    {
      string folderPath = Utility.SelectFolder(packDirectory);
      if (folderPath == null) return;
      packDirectory = folderPath;

      // Load pack
      try
      {
        curPack = LoadPack.Load(packDirectory);
        isSaved = false;

        // Finally update the form
        WritePackInfoToForm();
      }
      catch (Exception Ex) when (!Env.Debugging)
      {
        Utility.LogException(Ex);
        MessageBox.Show("Could not load the pack: " + Ex.Message, "Error loading a pack");
      }

      // Switch to first tab
      tabPack.SelectedIndex = 0;
    }

    /// <summary>
    /// Displays the about page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Form versionForm = new Form();
      versionForm.Width = 310;
      versionForm.Height = 170;
      versionForm.MaximizeBox = false;
      versionForm.ShowInTaskbar = false;
      versionForm.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      versionForm.Text = "NeoLemmix Level Pack Tool - About";

      versionForm.Show();

      using (Graphics aboutGraphics = versionForm.CreateGraphics())
      using (SolidBrush brush = new SolidBrush(Color.Black))
      using (Font font = new Font("Microsoft Sans Serif", 8))
      {
        for (int i = 0; i < C.VersionList.Count; i++)
        {
          aboutGraphics.DrawString(C.VersionList[i], font, brush, 6, 6 + 13 * i);
        }
      }
    }
  }
}
