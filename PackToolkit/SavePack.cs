﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace PackToolkit
{
  static class SavePack
  {
    static public bool Save(Pack pack, string defaultPath)
    {
      curPack = pack;

      // Get folder path to save to
      GetSaveFolder(defaultPath);
      if (string.IsNullOrEmpty(folderPath)) return false;

      // Get file-paths to write to
      List<string> sourceFiles = GetSourceFiles();
      // Get file paths in the target folder
      List<string> folderFiles = DirectoryFileSearch(folderPath);

      // Remove all unused files in the target folder
      if (folderFiles.Count > 0)
      {
        if (AskOverwriteFiles())
        {
          RemoveUnusedFiles(folderFiles, sourceFiles);
        }
        else
        {
          return false;
        }
      }

      // Add this folder, if it doesn't exist yet
      if (!Directory.Exists(folderPath))
      {
        Directory.CreateDirectory(folderPath);
      }

      // Write the global info files.
      WriteInfoFile();
      WriteRankNameFile();
      WriteResultTextFile();
      WriteMusicRotationFile();
      WriteLevelNameFiles();
      // Save all custom sprites and music.
      SaveGlobalSprites();
      SaveRankSprites();
      SaveMusics();
      // Finally save all levels.
      pack.SetTalismanIndex();
      SaveLevels();
      // And check whether there are now completely empty folders and delete them
      DeleteEmptyFolders();

      return true;
    }

    static string folderPath = string.Empty;
    static Pack curPack;

    /// <summary>
    /// Opens the file browser to select a folder to save the pack in.
    /// </summary>
    /// <returns></returns>
    static private void GetSaveFolder(string defaultPath)
    {
      bool hasFolderPath = false;
      do
      {
        folderPath = Utility.SelectFolder(defaultPath);

        if (folderPath == C.AppPathLevels)
        {
          string message = "Warning: It is encouraged to save your pack in a subfolder. Are you really sure to save the pack here?";
          DialogResult dialogResult = MessageBox.Show(message, "Save here?", MessageBoxButtons.YesNo);
          hasFolderPath = dialogResult == DialogResult.Yes;
        }
        else
        {
          hasFolderPath = true;
        }
      } while (!hasFolderPath);
    }

    /// <summary>
    /// Gets all files in the forder or any subfolders of folderPath.
    /// </summary>
    /// <param name="dirPath"></param>
    /// <returns></returns>
    static private List<string> DirectoryFileSearch(string dirPath)
    {
      List<string> filePaths = new List<string>();
      if (!Directory.Exists(dirPath)) return filePaths;

      filePaths.AddRange(Directory.GetFiles(dirPath));

      foreach (string subfolder in Directory.GetDirectories(dirPath))
      {
        filePaths.AddRange(DirectoryFileSearch(subfolder));
      }

      return filePaths;
    }

    /// <summary>
    /// List all source files, from which we have to read.
    /// </summary>
    /// <returns></returns>
    static private List<string> GetSourceFiles()
    {
      List<string> sourceFiles = new List<string>();

      // Add all level files
      foreach (Rank rank in curPack.Ranks)
      {
        sourceFiles.AddRange(rank.Levels.ConvertAll(level => level.FilePath));
        // And the rank images
        if (!string.IsNullOrWhiteSpace(rank.SpritePath)) sourceFiles.Add(rank.SpritePath);
      }

      // Add all sprite files
      sourceFiles.AddRange(curPack.Sprites.Values);
      // Add all music files
      sourceFiles.AddRange(curPack.Musics.ConvertAll(music => music.FilePath));

      // Ensure that everything are full file paths.
      sourceFiles.ForEach(file => file = Path.GetFullPath(file));

      return sourceFiles;
    }

    /// <summary>
    /// Asks the user whether to continue with possible data loss.
    /// </summary>
    /// <returns></returns>
    static private bool AskOverwriteFiles()
    {
      string text = "The selected folder contains already files. Saving the pack there will overwrite or delete the existing files. Continue?";
      return MessageBox.Show(text, "Overwrite Files?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
    }

    /// <summary>
    /// Deletes all files from folderFiles, that are not contained in sourceFiles.
    /// </summary>
    /// <param name="folderFiles"></param>
    /// <param name="sourceFiles"></param>
    static private void RemoveUnusedFiles(List<string> folderFiles, List<string> sourceFiles)
    {
      var removeFiles = folderFiles.Except(sourceFiles, new FilePathComparer()).ToList();
      removeFiles.ForEach(file => File.Delete(file));
    }

    /// <summary>
    /// Writes the global info file.
    /// </summary>
    static private void WriteInfoFile()
    {
      File.Create(folderPath + C.DirSep + "info.nxmi").Close();
      using (TextWriter textFile = new StreamWriter(folderPath + C.DirSep + "info.nxmi", true))
      {
        textFile.WriteLine("TITLE " + curPack.Title);
        textFile.WriteLine("AUTHOR " + curPack.Title2);
        textFile.WriteLine(" ");
        textFile.WriteLine("$SCROLLER ");
        foreach (string line in curPack.ScrollerText)
        {
          textFile.WriteLine("  LINE " + line);
        }
        textFile.WriteLine("$END ");
        var PanelSprites = new List<C.Sprite>() { C.Sprite.SkillPanel, C.Sprite.SkillCount, C.Sprite.SkillCountErase,
                                                          C.Sprite.EmptySkillSlot, C.Sprite.Minimap };
        if (curPack.Sprites.Keys.Intersect(PanelSprites).Count() > 0)
        {
          textFile.WriteLine(" ");
          textFile.WriteLine("PANEL ");
        }
      }
    }

    /// <summary>
    /// Writes the rank name file.
    /// </summary>
    static private void WriteRankNameFile()
    {
      // Global rank file
      File.Create(folderPath + C.DirSep + "levels.nxmi").Close();
      using (TextWriter textFile = new StreamWriter(folderPath + C.DirSep + "levels.nxmi", true))
      {
        textFile.WriteLine("BASE ");
        textFile.WriteLine(" ");
        foreach (Rank rank in curPack.Ranks)
        {
          textFile.WriteLine("$RANK ");
          textFile.WriteLine("  NAME " + rank.Name);
          textFile.WriteLine("  FOLDER " + Utility.MakeFileNameCompliant(rank.Name));
          textFile.WriteLine("$END ");
          textFile.WriteLine(" ");
        }
      }
    }

    /// <summary>
    /// Writes the global result text file.
    /// </summary>
    static private void WriteResultTextFile()
    {
      // Global result info file
      File.Create(folderPath + C.DirSep + "postview.nxmi").Close();
      using (TextWriter textFile = new StreamWriter(folderPath + C.DirSep + "postview.nxmi", true))
      {
        foreach (C.ResultTextType type in C.ResultTextTypeList) // reverses them to start from the worst condition
        {
          textFile.WriteLine("$RESULT ");
          switch (type)
          {
            case C.ResultTextType.All: textFile.WriteLine("  CONDITION 100%"); break;
            case C.ResultTextType.Great: textFile.WriteLine("  CONDITION +20%"); break;
            case C.ResultTextType.Good: textFile.WriteLine("  CONDITION +1"); break;
            case C.ResultTextType.Exact: textFile.WriteLine("  CONDITION +0"); break;
            case C.ResultTextType.OneShort: textFile.WriteLine("  CONDITION -1"); break;
            case C.ResultTextType.NearPass: textFile.WriteLine("  CONDITION -5"); break;
            case C.ResultTextType.Bad: textFile.WriteLine("  CONDITION -50%"); break;
            case C.ResultTextType.VeryBad: textFile.WriteLine("  CONDITION 1"); break;
            case C.ResultTextType.None: textFile.WriteLine("  CONDITION 0"); break;
          }
          textFile.WriteLine("  LINE " + curPack.ResultText[type][0]);
          textFile.WriteLine("  LINE " + curPack.ResultText[type][1]);
          textFile.WriteLine("$END ");
          textFile.WriteLine(" ");
        }
      }
    }

    /// <summary>
    /// Writes the music rotation file - which we don't support.
    /// </summary>
    static private void WriteMusicRotationFile()
    {
      if (curPack.MusicRotation == null || curPack.MusicRotation.Count == 0) return;

      File.Create(folderPath + C.DirSep + "music.nxmi").Close();
      using (TextWriter textFile = new StreamWriter(folderPath + C.DirSep + "music.nxmi", true))
      {
        foreach (string musicName in curPack.MusicRotation)
        {
          textFile.WriteLine("TRACK " + Path.GetFileNameWithoutExtension(musicName));
        }
      }
    }

    /// <summary>
    /// Writes for each rank the level name file.
    /// </summary>
    static private void WriteLevelNameFiles()
    {
      foreach (Rank rank in curPack.Ranks)
      {
        string rankPath = folderPath + C.DirSep + Utility.MakeFileNameCompliant(rank.Name);
        if (!Directory.Exists(rankPath)) Directory.CreateDirectory(rankPath);

        File.Create(rankPath + C.DirSep + "levels.nxmi").Close();
        using (TextWriter textFile = new StreamWriter(rankPath + C.DirSep + "levels.nxmi", true))
        {
          foreach (Level level in rank.Levels)
          {
            string name = Utility.MakeFileNameCompliant(Path.GetFileName(level.FilePath));
            textFile.WriteLine("LEVEL " + name);
          }
        }
      }
    }

    /// <summary>
    /// Saves all global sprites
    /// </summary>
    private static void SaveGlobalSprites()
    {
      foreach (C.Sprite sprite in curPack.Sprites.Keys.ToList())
      {
        string targetPath = folderPath + C.DirSep + C.PackSpritePath[sprite];
        SaveImage(curPack.Sprites[sprite], targetPath);
        curPack.Sprites[sprite] = targetPath;
      }
    }

    /// <summary>
    /// Saves the rank sprites in theic corresponding folders.
    /// </summary>
    private static void SaveRankSprites()
    {
      foreach (Rank rank in curPack.Ranks.FindAll(rank => !string.IsNullOrEmpty(rank.SpritePath)))
      {
        string targetPath = folderPath + C.DirSep + Utility.MakeFileNameCompliant(rank.Name) + C.DirSep + "rank_graphic.png";
        SaveImage(rank.SpritePath, targetPath);
        rank.SpritePath = targetPath;
      }
    }


    /// <summary>
    /// Saves an image from an original path at the target location, and deletes the old one, if it was present in the folderPath directory.
    /// </summary>
    /// <param name="origPath"></param>
    /// <param name="targetPath"></param>
    private static void SaveImage(string origPath, string targetPath)
    {
      if (origPath.Equals(targetPath)) return;
      File.Copy(origPath, targetPath);

      if (origPath.StartsWith(folderPath))
      {
        File.Delete(origPath);
      }
    }

    /// <summary>
    /// Saves all custom music files in the subfolder "music".
    /// </summary>
    private static void SaveMusics()
    {
      if (curPack.Musics.Count == 0) return;

      string targetFolder = folderPath + C.DirSep + "music";
      if (!Directory.Exists(targetFolder)) Directory.CreateDirectory(targetFolder);

      foreach (Music music in curPack.Musics)
      {
        // SaveImage can be used for music files, too
        string targetPath = targetFolder + C.DirSep + Path.GetFileName(music.FilePath);
        SaveImage(music.FilePath, targetPath);
        music.FilePath = targetPath;
      }
    }


    /// <summary>
    /// Saves all levels.
    /// </summary>
    private static void SaveLevels()
    {
      foreach (Rank rank in curPack.Ranks)
      {
        foreach (Level level in rank.Levels)
        {
          string targetPath = folderPath + C.DirSep + Utility.MakeFileNameCompliant(rank.Name)
                                         + C.DirSep + Utility.MakeFileNameCompliant(Path.GetFileName(level.FilePath));
          string tempPath = folderPath + C.DirSep + "temp.nxlv";

          // Move resp. copy the level to folderPath/temp.nxlv
          if (level.FilePath.StartsWith(folderPath))
          {
            File.Move(level.FilePath, tempPath);
          }
          else
          {
            File.Copy(level.FilePath, tempPath);
          }
          // Write correct level file to the targetPath
          level.FilePath = tempPath;
          SaveOneLevel(level, targetPath);
          level.FilePath = targetPath;
          // Delete the temp.nxlv
          File.Delete(folderPath + C.DirSep + "temp.nxlv");
        }
      }
    }

    /// <summary>
    /// Copies temp.nxlv and adds the additional infos as set in the toolkit.
    /// </summary>
    /// <param name="level"></param>
    /// <param name="targetPath"></param>
    private static void SaveOneLevel(Level level, string targetPath)
    {
      bool hasAddedTalisman = false;
      bool doDeleteLines = false;
      bool wasPreviousLineEmpty = false;

      File.Create(targetPath).Close();
      using (var fileStream = new StreamReader(folderPath + C.DirSep + "temp.nxlv"))
      using (TextWriter textFile = new StreamWriter(targetPath, true))
      {
        string line;
        while ((line = fileStream.ReadLine()) != null)
        {
          if (doDeleteLines)
          {
            // Check only for end of group to delete
            if (line.Trim().StartsWith("$END")) doDeleteLines = false;
          }
          else if (line.Trim().StartsWith("$TALISMAN") ||
                   line.Trim().StartsWith("$PRETEXT") ||
                   line.Trim().StartsWith("$POSTTEXT"))
          {
            // Delete this group
            doDeleteLines = true;
          }
          else if (!hasAddedTalisman &&
                  (line.Trim().StartsWith("$OBJECT") || line.Trim().StartsWith("#     Interactive objects")))
          {
            hasAddedTalisman = true;

            // Add pre-/post-text info first
            bool hasPreView = level.PreViewText != null && !string.IsNullOrWhiteSpace(string.Join("", level.PreViewText));
            bool hasPostView = level.PostViewText != null && !string.IsNullOrWhiteSpace(string.Join("", level.PostViewText));

            if (hasPreView)
            {
              textFile.WriteLine(" $PRETEXT ");
              foreach (string text in level.PreViewText)
              {
                textFile.WriteLine("   LINE " + text);
              }
              textFile.WriteLine(" $END ");
              textFile.WriteLine(" ");
            }
            if (hasPostView)
            {
              textFile.WriteLine(" $POSTTEXT ");
              foreach (string text in level.PostViewText)
              {
                textFile.WriteLine("   LINE " + text);
              }
              textFile.WriteLine(" $END ");
              textFile.WriteLine(" ");
            }

            // Add talisman info
            foreach (Talisman talisman in level.Talismans)
            {
              textFile.WriteLine(" $TALISMAN ");
              textFile.WriteLine("   TITLE " + talisman.Title);
              textFile.WriteLine("   ID " + talisman.ID);
              textFile.WriteLine("   COLOR " + talisman.AwardType.ToString());
              foreach (C.TalismanReq requirement in talisman.Requirements.Keys)
              {
                if (requirement != C.TalismanReq.UseOnlySkill)
                {
                  textFile.WriteLine("   " + C.TalismanKeys[requirement] + " " + talisman.Requirements[requirement].ToString());
                }
                else
                {
                  textFile.WriteLine("   " + C.TalismanKeys[requirement] + " " + C.TalismanSkills[talisman.Requirements[requirement]]);
                }
              }
              textFile.WriteLine(" $END ");
              textFile.WriteLine(" ");
            }

            // Finally copy the line
            textFile.WriteLine(line);
          }
          else
          {
            // Just copy the line (but avoid two empty lines next to each other
            if (!wasPreviousLineEmpty || !string.IsNullOrWhiteSpace(line))
            {
              textFile.WriteLine(line);
            }
            wasPreviousLineEmpty = string.IsNullOrWhiteSpace(line);
          }
        }
      }
    }

    /// <summary>
    /// Removes all empty folders in the pack directory.
    /// </summary>
    static private void DeleteEmptyFolders()
    {
      var subdirs = Directory.GetDirectories(folderPath);
      var deletedirs = subdirs.Where(dir => Directory.GetFiles(dir).Count() == 0);
      foreach (string dir in deletedirs)
      {
        Directory.Delete(dir, true);
      }
    }
  }
}
