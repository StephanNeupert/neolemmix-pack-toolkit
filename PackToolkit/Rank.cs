﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PackToolkit
{
  /// <summary>
  /// A whole rank of levels.
  /// </summary>
  class Rank
  {
    public Rank(string name)
    {
      Name = name;
      Levels = new List<Level>();
    }

    public string Name;
    public List<Level> Levels;
    public string SpritePath;
  }

  /// <summary>
  /// One level and its associated information.
  /// </summary>
  class Level
  {
    public Level(string filePath, Pack curPack)
    {
      FilePath = filePath;
      Name = "Unknown Level";
      Talismans = new List<Talisman>();
      PreViewText = new List<string>();
      PostViewText = new List<string>();
      ReadInfoFromFile(curPack);
    }

    public string FilePath;
    public string Name { get; private set; }
    public List<Talisman> Talismans;
    public List<string> PreViewText;
    public List<string> PostViewText;

    /// <summary>
    /// Reads the name, pre-, postview texts and talismans from a level file.
    /// </summary>
    private void ReadInfoFromFile(Pack curPack)
    {
      FileParser parser = null;
      try
      {
        parser = new FileParser(FilePath);
      }
      catch (Exception Ex)
      {
        MessageBox.Show(Ex.Message, "File corrupt: " + FilePath);
        parser?.DisposeStreamReader();
        return;
      }

      List<FileLine> fileLines;
      while ((fileLines = parser.GetNextLines()) != null)
      {
        System.Diagnostics.Debug.Assert(fileLines.Count > 0, "FileParser returned empty list.");

        FileLine line = fileLines[0];
        switch (line.Key)
        {
          case "TITLE": Name = line.Text; break;
          case "PRETEXT":
            {
              PreViewText = fileLines.FindAll(text => text.Key.ToUpper().Equals("LINE"))
                                     .ConvertAll(text => text.Text);
              break;
            }
          case "POSTTEXT":
            {
              PostViewText = fileLines.FindAll(text => text.Key.ToUpper().Equals("LINE"))
                                      .ConvertAll(text => text.Text);
              break;
            }
          case "TALISMAN":
            {
              Talismans.Add(ReadTalisman(fileLines, curPack));
              break;
            }
        }
      }
      parser.DisposeStreamReader();
    }

    /// <summary>
    /// Reads the talisman information from a group of fileLines.
    /// </summary>
    /// <param name="fileLines"></param>
    /// <param name="curPack"></param>
    /// <returns></returns>
    private Talisman ReadTalisman(List<FileLine> fileLines, Pack curPack)
    {
      Talisman talisman = new Talisman(curPack, this);

      foreach (FileLine line in fileLines)
      {
        switch (line.Key)
        {
          case "TITLE": talisman.Title = line.Text; break;
          case "COLOR": talisman.AwardType = Utility.ParseEnum<C.TalismanType>(line.Text); break;
          case "ID": talisman.ID = line.Value; break;
          default:
            {
              if (C.TalismanKeys.Values.Contains(line.Key))
              {
                C.TalismanReq requirement = C.TalismanKeys.First(pair => pair.Value.Equals(line.Key)).Key;
                talisman.Requirements[requirement] = line.Value;
              }
              break;
            }
        }
      }

      return talisman;
    }
  }


  /// <summary>
  /// A music file.
  /// </summary>
  class Music : IEquatable<Music>, IComparable
  {
    public Music(string filePath)
    {
      FilePath = filePath;
      Name = System.IO.Path.GetFileNameWithoutExtension(FilePath);
    }

    public string FilePath;
    public string Name { get; private set; }

    public bool Equals(Music otherMusic)
    {
      return this.Name.Equals(otherMusic.Name);
    }

    public int CompareTo(object otherMusic)
    {
      try
      {
        return this.Name.CompareTo((otherMusic as Music).Name);
      }
      catch
      {
        return 0;
      }
    }
  }
}
