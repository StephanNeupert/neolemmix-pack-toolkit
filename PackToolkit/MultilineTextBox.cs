﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PackToolkit
{
    public class MultilineTextBox : ComboBox
    {
        public MultilineTextBox() : base()
        {
            selectedIndex = 0;
            isFirstTextChange = true;
            this.SelectedIndexChanged += new EventHandler(NewSelectedIndexChanged);
            this.TextChanged += new EventHandler(NewTextChanged);
            this.DropDown += new EventHandler(NewDropDown);
            this.DropDownClosed += new EventHandler(NewDropDownClosed);
        }

        int selectedIndex;
        bool isFirstTextChange;

        /// <summary>
        /// Remeber selected item 
        /// </summary>
        private void NewSelectedIndexChanged(object sender, EventArgs e)
        {
            selectedIndex = this.SelectedIndex;
            isFirstTextChange = true;
        }

        /// <summary>
        /// Update selected item according to text
        /// </summary>
        private void NewTextChanged(object sender, EventArgs e)
        {
            if (isFirstTextChange)
            {
                isFirstTextChange = false;
            }
            else
            {
                if (this.Items.Count > selectedIndex)
                {
                    this.Items[selectedIndex] = this.Text;
                }
            }
        }

        /// <summary>
        /// Don't change the text, while the drop-down is shown
        /// </summary>
        private void NewDropDown(object sender, EventArgs e)
        {
            isFirstTextChange = true;
        }

        /// <summary>
        /// Change text again, when drop-down is closed
        /// </summary>
        private void NewDropDownClosed(object sender, EventArgs e)
        {
            if (selectedIndex == this.SelectedIndex)
            {
                isFirstTextChange = false;
            }
        }

    }
}
