﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace PackToolkit
{
  /// <summary>
  /// Here is all the code related to actions performed on the tab "Sprites"
  /// </summary>
  public partial class FormMain
  {
    /// <summary>
    /// Creates a list of all keys for the customizable sprites.
    /// </summary>
    /// <returns></returns>
    private List<string> GetSpriteNames()
    {
      List<string> spriteNames = curPack.GetSpriteNames();

      foreach (C.Sprite sprite in C.SpriteArray)
      {
        spriteNames.Add(C.SpriteName[sprite]);
      }

      return spriteNames;
    }

    /// <summary>
    /// Returns the rank corresponding to the key text.
    /// </summary>
    /// <param name="keyText"></param>
    /// <returns></returns>
    private Rank GetRankFromSpriteName(string keyText)
    {
      if (!GetSpriteNames().Contains(keyText) || !keyText.StartsWith("Rank")) return null;

      return curPack.Ranks[GetSpriteNames().IndexOf(keyText)];
    }

    /// <summary>
    /// Set the combobox values correctly according to existing ranks, if entering this tab.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void PageSprites_Enter(object sender, EventArgs e)
    {
      cmbSprites.Items.Clear();
      foreach (string spriteName in GetSpriteNames())
      {
        cmbSprites.Items.Add(spriteName);
      }

      cmbSprites.Text = cmbSprites.Items[0].ToString();
      SetSpriteButtonActive();
    }

    /// <summary>
    /// Try to display the corresponding image to the new sprite name.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void cmbSprites_SelectedValueChanged(object sender, EventArgs e)
    {
      string keyText = cmbSprites.SelectedItem.ToString().Trim();
      string filePath = null;
      if (!GetSpriteNames().Contains(keyText))
      {
        // do nothing
      }
      else if (keyText.StartsWith("Rank"))
      {
        Rank rank = GetRankFromSpriteName(keyText);
        filePath = rank?.SpritePath ?? C.DefaultRankSprite;
      }
      else
      {
        C.Sprite sprite = C.GetSpriteFromKey(keyText);
        filePath = curPack.Sprites.ContainsKey(sprite) ? curPack.Sprites[sprite]
                                                       : C.GetDefaultSpritePath(sprite);
      }

      LoadSprite(filePath);
      SetSpriteButtonActive();
    }


    /// <summary>
    /// Loads a sprite image and displays it in picBoxSprite.
    /// </summary>
    /// <param name="filePath"></param>
    private void LoadSprite(string filePath)
    {
      if (filePath == null || !File.Exists(filePath))
      {
        picBoxSprite.Image = null;
      }
      else
      {
        picBoxSprite.Image = Utility.CreateBitmapFromFile(filePath);
      }
    }

    /// <summary>
    /// Adds a custom sprite image corresponding that the user selects in a file browser.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butSpriteAdd_Click(object sender, EventArgs e)
    {
      string keyText = cmbSprites.Text;
      if (!GetSpriteNames().Contains(keyText)) return;

      string filter = "Image files (*.png)|*.png";
      string filePath = Utility.SelectFiles(filter, false).FirstOrDefault();

      if (filePath == null || !File.Exists(filePath)) return;

      // Add image path tho the sprite dictionary
      if (keyText.StartsWith("Rank"))
      {
        Rank rank = GetRankFromSpriteName(keyText);
        rank.SpritePath = keyText;
      }
      else
      {
        curPack.Sprites[C.GetSpriteFromKey(keyText)] = filePath;
      }

      // Display selected image and set active buttons correctly.
      LoadSprite(filePath);
      SetSpriteButtonActive();
      isSaved = false;
    }

    /// <summary>
    /// Removes a custom sprite image.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butSpriteDelete_Click(object sender, EventArgs e)
    {
      string keyText = cmbSprites.Text;
      if (!GetSpriteNames().Contains(keyText)) return;

      if (keyText.StartsWith("Rank"))
      {
        Rank rank = GetRankFromSpriteName(keyText);
        rank.SpritePath = null;
      }
      else
      {
        C.Sprite sprite = C.GetSpriteFromKey(keyText);
        curPack.Sprites.Remove(sprite);
      }

      picBoxSprite.Image = null;
      isSaved = false;
    }

    /// <summary>
    /// Checks whether there is a sprite to delete, and sets the button accordingly.
    /// </summary>
    private void SetSpriteButtonActive()
    {
      string keyText = cmbSprites.Text.Trim();
      if (!GetSpriteNames().Contains(keyText))
      {
        butSpriteAdd.Enabled = false;
        butSpriteDelete.Enabled = false;
      }
      else if (keyText.StartsWith("Rank"))
      {
        Rank rank = GetRankFromSpriteName(keyText);
        butSpriteDelete.Enabled = (rank?.SpritePath != null);
        butSpriteAdd.Enabled = (rank != null);
      }
      else
      {
        C.Sprite sprite = C.GetSpriteFromKey(keyText);
        butSpriteDelete.Enabled = curPack.Sprites.ContainsKey(sprite);
        butSpriteAdd.Enabled = true;
      }
    }
  }
}
