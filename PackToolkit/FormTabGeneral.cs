﻿using System;
using System.Linq;


namespace PackToolkit
{
    /// <summary>
    /// Here is all the code related to actions performed on the tab "General"
    /// </summary>
    public partial class FormMain
    {
        private void txtTitle_Leave(object sender, EventArgs e)
        {
            curPack.Title = txtTitle.Text;
            isSaved = false;
        }

        private void txtTitle2_Leave(object sender, EventArgs e)
        {
            curPack.Title2 = txtTitle2.Text;
            isSaved = false;
        }

        private void cmbScrollerText_Leave(object sender, EventArgs e)
        {
            curPack.ScrollerText = cmbScrollerText.Items.Cast<string>().ToArray();
            isSaved = false;
        }

        private void cmbResultText_Leave(object sender, EventArgs e)
        {
            C.ResultTextType resultType = ResultTextBoxes.First(pair => pair.Value.Equals(sender as MultilineTextBox)).Key;
            MultilineTextBox resultBox = ResultTextBoxes[resultType];
            curPack.ResultText[resultType] = resultBox.Items.Cast<string>().ToArray();
            isSaved = false;
        }
    }


}
