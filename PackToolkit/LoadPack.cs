﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PackToolkit
{
  static class LoadPack
  {
    /// <summary>
    /// Loads a NeoLemmix pack from a given folder.
    /// </summary>
    /// <param name="folderPath"></param>
    /// <returns></returns>
    static public Pack Load(string folderPath)
    {
      Pack newPack = new Pack();

      // Load all general info
      LoadTitleInfo(newPack, folderPath);
      LoadResultTextInfo(newPack, folderPath);
      var rankFolders = LoadRankInfo(newPack, folderPath);
      // Load all rank specific info
      foreach (Rank rank in newPack.Ranks)
      {
        LoadLevelInfo(rank, rankFolders[rank], newPack);
      }
      // Load all general sprites
      LoadGeneralSprites(newPack, folderPath);
      // and finally the music
      LoadMusic(newPack, folderPath);
      LoadMusicRotation(newPack, folderPath);

      return newPack;
    }

    /// <summary>
    /// Loads the title information from the file.
    /// </summary>
    /// <param name="pack"></param>
    /// <param name="folderPath"></param>
    static private void LoadTitleInfo(Pack pack, string folderPath)
    {
      string filePath = folderPath + C.DirSep + "info.nxmi";
      if (!File.Exists(filePath)) return;

      FileParser parser = new FileParser(filePath);
      List<FileLine> fileLines;
      while ((fileLines = parser.GetNextLines()) != null)
      {
        FileLine line = fileLines[0];
        switch (line.Key)
        {
          case "TITLE": pack.Title = line.Text; break;
          case "AUTHOR": pack.Title2 = line.Text; break;
          case "SCROLLER":
            {
              pack.ScrollerText = fileLines.FindAll(l => fileLines.IndexOf(l) > 0)
                                           .ConvertAll(l => l.Text)
                                           .ToArray();
              break;
            }
        }
      }
      parser.DisposeStreamReader();
    }

    /// <summary>
    /// Loads the result texts for the postview screen.
    /// </summary>
    /// <param name="pack"></param>
    /// <param name="folderPath"></param>
    static private void LoadResultTextInfo(Pack pack, string folderPath)
    {
      string filePath = folderPath + C.DirSep + "postview.nxmi";
      if (!File.Exists(filePath)) return;

      FileParser parser = new FileParser(filePath);
      List<FileLine> fileLines;
      while ((fileLines = parser.GetNextLines()) != null)
      {
        if (fileLines[0].Key == "RESULT")
        {
          FileLine line = fileLines.Find(l => l.Key == "CONDITION");
          C.ResultTextType resultType;
          switch (line.Text.Trim())
          {
            case "100%": resultType = C.ResultTextType.All; break;
            case "+20%": resultType = C.ResultTextType.Great; break;
            case "+1": resultType = C.ResultTextType.Good; break;
            case "+0": resultType = C.ResultTextType.Exact; break;
            case "-1": resultType = C.ResultTextType.OneShort; break;
            case "-2":
            case "-5": resultType = C.ResultTextType.NearPass; break;
            case "-10%":
            case "-50%": resultType = C.ResultTextType.Bad; break;
            case "1": resultType = C.ResultTextType.VeryBad; break;
            case "0": resultType = C.ResultTextType.None; break;
            default: resultType = C.ResultTextType.None; break;
          }
          pack.ResultText[resultType] = fileLines.FindAll(l => l.Key == "LINE")
                                                 .ConvertAll(l => l.Text)
                                                 .ToArray();
        }
      }
      parser.DisposeStreamReader();
    }

    /// <summary>
    /// Loads the rank names.
    /// </summary>
    /// <param name="pack"></param>
    /// <param name="folderPath"></param>
    /// <returns></returns>
    static private Dictionary<Rank, string> LoadRankInfo(Pack pack, string folderPath)
    {
      pack.Ranks.Clear();

      string filePath = folderPath + C.DirSep + "levels.nxmi";
      Dictionary<Rank, string> rankFolders = new Dictionary<Rank, string>();

      if (!File.Exists(filePath))
      {
        var rankNames = Directory.GetDirectories(folderPath);
        foreach (string name in rankNames)
        {
          if (!name.ToUpper().Equals("MUSIC"))
          {
            Rank newRank = new Rank(name);
            pack.Ranks.Add(newRank);
            rankFolders[newRank] = folderPath + C.DirSep + name;
          }
        }
      }
      else
      {
        FileParser parser = new FileParser(filePath);
        List<FileLine> fileLines;
        while ((fileLines = parser.GetNextLines()) != null)
        {
          if (fileLines[0].Key == "RANK")
          {
            string name = fileLines.Find(l => l.Key == "NAME").Text;
            Rank newRank = new Rank(name);
            pack.Ranks.Add(newRank);

            string folderName = fileLines.FirstOrDefault(l => l.Key == "FOLDER")?.Text ?? name;
            rankFolders[newRank] = folderPath + C.DirSep + folderName;
          }
        }
        parser.DisposeStreamReader();
      }

      return rankFolders;
    }

    /// <summary>
    /// Loads the rank-specific information and adds its levels.
    /// </summary>
    /// <param name="rank"></param>
    /// <param name="rankPath"></param>
    static private void LoadLevelInfo(Rank rank, string rankPath, Pack pack)
    {
      // Add levels
      foreach (string levelPath in LoadLevelList(rankPath))
      {
        rank.Levels.Add(new Level(levelPath, pack));
      }
      // Add rank sign
      string signPath = rankPath + C.DirSep + "rank_graphic.png";
      if (File.Exists(signPath))
      {
        rank.SpritePath = signPath;
      }
    }

    /// <summary>
    /// Reads the level, either from the levels.nxmi file, or the list of level in the directory.
    /// </summary>
    /// <param name="rankPath"></param>
    /// <returns></returns>
    static private List<string> LoadLevelList(string rankPath)
    {
      string filePath = rankPath + C.DirSep + "levels.nxmi";
      List<string> levelPaths;

      if (!File.Exists(filePath))
      {
        levelPaths = Directory.GetFiles(rankPath, "*.nxlv").ToList();
      }
      else
      {
        levelPaths = new List<string>();
        FileParser parser = new FileParser(filePath);
        List<FileLine> fileLines;
        while ((fileLines = parser.GetNextLines()) != null)
        {
          if (fileLines[0].Key == "LEVEL")
          {
            levelPaths.Add(rankPath + C.DirSep + fileLines[0].Text);
          }
        }
        parser.DisposeStreamReader();
      }

      return levelPaths;
    }

    /// <summary>
    /// Loads the general sprites, if available.
    /// </summary>
    /// <param name="pack"></param>
    /// <param name="folderPath"></param>
    static private void LoadGeneralSprites(Pack pack, string folderPath)
    {
      foreach (C.Sprite sprite in C.SpriteArray)
      {
        string path = folderPath + C.DirSep + C.PackSpritePath[sprite];
        if (File.Exists(path))
        {
          pack.Sprites[sprite] = path;
        }
      }
    }

    /// <summary>
    /// Loads all music files.
    /// </summary>
    /// <param name="pack"></param>
    /// <param name="folderPath"></param>
    static private void LoadMusic(Pack pack, string folderPath)
    {
      string musicPath = folderPath + C.DirSep + "music";
      if (!Directory.Exists(musicPath)) return;

      var musicNames = Directory.GetFiles(musicPath).ToList()
                                .FindAll(file => Path.GetExtension(file).In(".ogg", ".mp3", ".mod", ".it", ".wav", ".xm"));
      pack.Musics = musicNames.ConvertAll(music => new Music(music));
    }

    /// <summary>
    /// Loads the music rotation file
    /// </summary>
    /// <param name="pack"></param>
    /// <param name="folderPath"></param>
    static private void LoadMusicRotation(Pack pack, string folderPath)
    {
      string musicPath = folderPath + C.DirSep + "music.nxmi";
      if (!File.Exists(musicPath)) return;

      pack.MusicRotation = new List<string>();
      FileParser parser = new FileParser(musicPath);
      List<FileLine> fileLines;
      while ((fileLines = parser.GetNextLines()) != null)
      {
        if (fileLines[0].Key == "TRACK")
        {
          pack.MusicRotation.Add(fileLines[0].Text);
        }
      }
      parser.DisposeStreamReader();
    }


  }
}
