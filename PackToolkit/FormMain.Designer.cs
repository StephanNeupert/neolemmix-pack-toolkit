﻿namespace PackToolkit
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.tabPack = new System.Windows.Forms.TabControl();
      this.PageGeneral = new System.Windows.Forms.TabPage();
      this.cmbResultText9 = new PackToolkit.MultilineTextBox();
      this.lblResultText9 = new System.Windows.Forms.Label();
      this.cmbResultText8 = new PackToolkit.MultilineTextBox();
      this.lblResultText8 = new System.Windows.Forms.Label();
      this.cmbResultText7 = new PackToolkit.MultilineTextBox();
      this.lblResultText7 = new System.Windows.Forms.Label();
      this.cmbResultText6 = new PackToolkit.MultilineTextBox();
      this.lblResultText6 = new System.Windows.Forms.Label();
      this.cmbResultText5 = new PackToolkit.MultilineTextBox();
      this.lblResultText5 = new System.Windows.Forms.Label();
      this.cmbResultText4 = new PackToolkit.MultilineTextBox();
      this.lblResultText4 = new System.Windows.Forms.Label();
      this.cmbResultText3 = new PackToolkit.MultilineTextBox();
      this.lblResultText3 = new System.Windows.Forms.Label();
      this.cmbResultText2 = new PackToolkit.MultilineTextBox();
      this.lblResultText2 = new System.Windows.Forms.Label();
      this.lblResultTexts = new System.Windows.Forms.Label();
      this.cmbResultText1 = new PackToolkit.MultilineTextBox();
      this.lblResultText1 = new System.Windows.Forms.Label();
      this.cmbScrollerText = new PackToolkit.MultilineTextBox();
      this.lblScrollerText = new System.Windows.Forms.Label();
      this.txtTitle2 = new System.Windows.Forms.TextBox();
      this.txtTitle = new System.Windows.Forms.TextBox();
      this.lblTitle2 = new System.Windows.Forms.Label();
      this.lblTitle = new System.Windows.Forms.Label();
      this.PageLevels = new System.Windows.Forms.TabPage();
      this.butLevelReplace = new System.Windows.Forms.Button();
      this.butLevelRankDown = new System.Windows.Forms.Button();
      this.butLevelRankUp = new System.Windows.Forms.Button();
      this.listTalismans = new System.Windows.Forms.ListBox();
      this.listRanks = new System.Windows.Forms.ListBox();
      this.txtRankName = new System.Windows.Forms.TextBox();
      this.lblRankName = new System.Windows.Forms.Label();
      this.listLevels = new System.Windows.Forms.ListBox();
      this.butTalismanChange = new System.Windows.Forms.Button();
      this.butTalismanDelete = new System.Windows.Forms.Button();
      this.butTalismanAdd = new System.Windows.Forms.Button();
      this.butPostview = new System.Windows.Forms.Button();
      this.butPreview = new System.Windows.Forms.Button();
      this.butLevelDown = new System.Windows.Forms.Button();
      this.butLevelUp = new System.Windows.Forms.Button();
      this.butLevelDelete = new System.Windows.Forms.Button();
      this.butLevelAdd = new System.Windows.Forms.Button();
      this.butRankDown = new System.Windows.Forms.Button();
      this.butRankUp = new System.Windows.Forms.Button();
      this.butRankDelete = new System.Windows.Forms.Button();
      this.butRankAdd = new System.Windows.Forms.Button();
      this.PageMusic = new System.Windows.Forms.TabPage();
      this.listMusic = new System.Windows.Forms.ListBox();
      this.butMusicDelete = new System.Windows.Forms.Button();
      this.butMusicAdd = new System.Windows.Forms.Button();
      this.PageSprites = new System.Windows.Forms.TabPage();
      this.picBoxSprite = new System.Windows.Forms.PictureBox();
      this.butSpriteDelete = new System.Windows.Forms.Button();
      this.butSpriteAdd = new System.Windows.Forms.Button();
      this.cmbSprites = new System.Windows.Forms.ComboBox();
      this.menuStrip1.SuspendLayout();
      this.tabPack.SuspendLayout();
      this.PageGeneral.SuspendLayout();
      this.PageLevels.SuspendLayout();
      this.PageMusic.SuspendLayout();
      this.PageSprites.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picBoxSprite)).BeginInit();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.aboutToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(377, 24);
      this.menuStrip1.TabIndex = 0;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // exitToolStripMenuItem
      // 
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
      this.exitToolStripMenuItem.Text = "Exit";
      this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
      // 
      // loadToolStripMenuItem
      // 
      this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
      this.loadToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
      this.loadToolStripMenuItem.Text = "Load";
      this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
      // 
      // saveToolStripMenuItem
      // 
      this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
      this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
      this.saveToolStripMenuItem.Text = "Save";
      this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
      // 
      // aboutToolStripMenuItem
      // 
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
      this.aboutToolStripMenuItem.Text = "About";
      this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
      // 
      // tabPack
      // 
      this.tabPack.Controls.Add(this.PageGeneral);
      this.tabPack.Controls.Add(this.PageLevels);
      this.tabPack.Controls.Add(this.PageMusic);
      this.tabPack.Controls.Add(this.PageSprites);
      this.tabPack.Location = new System.Drawing.Point(0, 27);
      this.tabPack.Name = "tabPack";
      this.tabPack.SelectedIndex = 0;
      this.tabPack.Size = new System.Drawing.Size(374, 374);
      this.tabPack.TabIndex = 1;
      this.tabPack.Enter += new System.EventHandler(this.tabPack_Enter);
      // 
      // PageGeneral
      // 
      this.PageGeneral.Controls.Add(this.cmbResultText9);
      this.PageGeneral.Controls.Add(this.lblResultText9);
      this.PageGeneral.Controls.Add(this.cmbResultText8);
      this.PageGeneral.Controls.Add(this.lblResultText8);
      this.PageGeneral.Controls.Add(this.cmbResultText7);
      this.PageGeneral.Controls.Add(this.lblResultText7);
      this.PageGeneral.Controls.Add(this.cmbResultText6);
      this.PageGeneral.Controls.Add(this.lblResultText6);
      this.PageGeneral.Controls.Add(this.cmbResultText5);
      this.PageGeneral.Controls.Add(this.lblResultText5);
      this.PageGeneral.Controls.Add(this.cmbResultText4);
      this.PageGeneral.Controls.Add(this.lblResultText4);
      this.PageGeneral.Controls.Add(this.cmbResultText3);
      this.PageGeneral.Controls.Add(this.lblResultText3);
      this.PageGeneral.Controls.Add(this.cmbResultText2);
      this.PageGeneral.Controls.Add(this.lblResultText2);
      this.PageGeneral.Controls.Add(this.lblResultTexts);
      this.PageGeneral.Controls.Add(this.cmbResultText1);
      this.PageGeneral.Controls.Add(this.lblResultText1);
      this.PageGeneral.Controls.Add(this.cmbScrollerText);
      this.PageGeneral.Controls.Add(this.lblScrollerText);
      this.PageGeneral.Controls.Add(this.txtTitle2);
      this.PageGeneral.Controls.Add(this.txtTitle);
      this.PageGeneral.Controls.Add(this.lblTitle2);
      this.PageGeneral.Controls.Add(this.lblTitle);
      this.PageGeneral.Location = new System.Drawing.Point(4, 22);
      this.PageGeneral.Name = "PageGeneral";
      this.PageGeneral.Padding = new System.Windows.Forms.Padding(3);
      this.PageGeneral.Size = new System.Drawing.Size(366, 348);
      this.PageGeneral.TabIndex = 0;
      this.PageGeneral.Text = "General";
      this.PageGeneral.UseVisualStyleBackColor = true;
      // 
      // cmbResultText9
      // 
      this.cmbResultText9.FormattingEnabled = true;
      this.cmbResultText9.Location = new System.Drawing.Point(110, 312);
      this.cmbResultText9.MaxDropDownItems = 2;
      this.cmbResultText9.Name = "cmbResultText9";
      this.cmbResultText9.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText9.TabIndex = 24;
      // 
      // lblResultText9
      // 
      this.lblResultText9.AutoSize = true;
      this.lblResultText9.Location = new System.Drawing.Point(8, 315);
      this.lblResultText9.Name = "lblResultText9";
      this.lblResultText9.Size = new System.Drawing.Size(67, 13);
      this.lblResultText9.TabIndex = 23;
      this.lblResultText9.Text = "None Saved";
      // 
      // cmbResultText8
      // 
      this.cmbResultText8.FormattingEnabled = true;
      this.cmbResultText8.Location = new System.Drawing.Point(110, 285);
      this.cmbResultText8.MaxDropDownItems = 2;
      this.cmbResultText8.Name = "cmbResultText8";
      this.cmbResultText8.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText8.TabIndex = 22;
      // 
      // lblResultText8
      // 
      this.lblResultText8.AutoSize = true;
      this.lblResultText8.Location = new System.Drawing.Point(8, 288);
      this.lblResultText8.Name = "lblResultText8";
      this.lblResultText8.Size = new System.Drawing.Size(50, 13);
      this.lblResultText8.TabIndex = 21;
      this.lblResultText8.Text = "Very Bad";
      // 
      // cmbResultText7
      // 
      this.cmbResultText7.FormattingEnabled = true;
      this.cmbResultText7.Location = new System.Drawing.Point(110, 258);
      this.cmbResultText7.MaxDropDownItems = 2;
      this.cmbResultText7.Name = "cmbResultText7";
      this.cmbResultText7.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText7.TabIndex = 20;
      // 
      // lblResultText7
      // 
      this.lblResultText7.AutoSize = true;
      this.lblResultText7.Location = new System.Drawing.Point(8, 261);
      this.lblResultText7.Name = "lblResultText7";
      this.lblResultText7.Size = new System.Drawing.Size(26, 13);
      this.lblResultText7.TabIndex = 19;
      this.lblResultText7.Text = "Bad";
      // 
      // cmbResultText6
      // 
      this.cmbResultText6.FormattingEnabled = true;
      this.cmbResultText6.Location = new System.Drawing.Point(110, 231);
      this.cmbResultText6.MaxDropDownItems = 2;
      this.cmbResultText6.Name = "cmbResultText6";
      this.cmbResultText6.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText6.TabIndex = 18;
      // 
      // lblResultText6
      // 
      this.lblResultText6.AutoSize = true;
      this.lblResultText6.Location = new System.Drawing.Point(8, 234);
      this.lblResultText6.Name = "lblResultText6";
      this.lblResultText6.Size = new System.Drawing.Size(56, 13);
      this.lblResultText6.TabIndex = 17;
      this.lblResultText6.Text = "Near Pass";
      // 
      // cmbResultText5
      // 
      this.cmbResultText5.FormattingEnabled = true;
      this.cmbResultText5.Location = new System.Drawing.Point(110, 204);
      this.cmbResultText5.MaxDropDownItems = 2;
      this.cmbResultText5.Name = "cmbResultText5";
      this.cmbResultText5.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText5.TabIndex = 16;
      // 
      // lblResultText5
      // 
      this.lblResultText5.AutoSize = true;
      this.lblResultText5.Location = new System.Drawing.Point(8, 207);
      this.lblResultText5.Name = "lblResultText5";
      this.lblResultText5.Size = new System.Drawing.Size(55, 13);
      this.lblResultText5.TabIndex = 15;
      this.lblResultText5.Text = "One Short";
      // 
      // cmbResultText4
      // 
      this.cmbResultText4.FormattingEnabled = true;
      this.cmbResultText4.Location = new System.Drawing.Point(110, 177);
      this.cmbResultText4.MaxDropDownItems = 2;
      this.cmbResultText4.Name = "cmbResultText4";
      this.cmbResultText4.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText4.TabIndex = 14;
      // 
      // lblResultText4
      // 
      this.lblResultText4.AutoSize = true;
      this.lblResultText4.Location = new System.Drawing.Point(8, 180);
      this.lblResultText4.Name = "lblResultText4";
      this.lblResultText4.Size = new System.Drawing.Size(60, 13);
      this.lblResultText4.TabIndex = 13;
      this.lblResultText4.Text = "Exact Pass";
      // 
      // cmbResultText3
      // 
      this.cmbResultText3.FormattingEnabled = true;
      this.cmbResultText3.Location = new System.Drawing.Point(110, 150);
      this.cmbResultText3.MaxDropDownItems = 2;
      this.cmbResultText3.Name = "cmbResultText3";
      this.cmbResultText3.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText3.TabIndex = 12;
      // 
      // lblResultText3
      // 
      this.lblResultText3.AutoSize = true;
      this.lblResultText3.Location = new System.Drawing.Point(8, 153);
      this.lblResultText3.Name = "lblResultText3";
      this.lblResultText3.Size = new System.Drawing.Size(33, 13);
      this.lblResultText3.TabIndex = 11;
      this.lblResultText3.Text = "Good";
      // 
      // cmbResultText2
      // 
      this.cmbResultText2.FormattingEnabled = true;
      this.cmbResultText2.Location = new System.Drawing.Point(110, 123);
      this.cmbResultText2.MaxDropDownItems = 2;
      this.cmbResultText2.Name = "cmbResultText2";
      this.cmbResultText2.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText2.TabIndex = 10;
      this.cmbResultText2.Leave += new System.EventHandler(this.cmbResultText_Leave);
      // 
      // lblResultText2
      // 
      this.lblResultText2.AutoSize = true;
      this.lblResultText2.Location = new System.Drawing.Point(8, 126);
      this.lblResultText2.Name = "lblResultText2";
      this.lblResultText2.Size = new System.Drawing.Size(33, 13);
      this.lblResultText2.TabIndex = 9;
      this.lblResultText2.Text = "Great";
      // 
      // lblResultTexts
      // 
      this.lblResultTexts.AutoSize = true;
      this.lblResultTexts.Location = new System.Drawing.Point(7, 77);
      this.lblResultTexts.Name = "lblResultTexts";
      this.lblResultTexts.Size = new System.Drawing.Size(117, 13);
      this.lblResultTexts.TabIndex = 8;
      this.lblResultTexts.Text = "Level Result Messages";
      // 
      // cmbResultText1
      // 
      this.cmbResultText1.FormattingEnabled = true;
      this.cmbResultText1.Location = new System.Drawing.Point(110, 97);
      this.cmbResultText1.MaxDropDownItems = 2;
      this.cmbResultText1.Name = "cmbResultText1";
      this.cmbResultText1.Size = new System.Drawing.Size(250, 21);
      this.cmbResultText1.TabIndex = 7;
      this.cmbResultText1.Leave += new System.EventHandler(this.cmbResultText_Leave);
      // 
      // lblResultText1
      // 
      this.lblResultText1.AutoSize = true;
      this.lblResultText1.Location = new System.Drawing.Point(8, 99);
      this.lblResultText1.Name = "lblResultText1";
      this.lblResultText1.Size = new System.Drawing.Size(46, 13);
      this.lblResultText1.TabIndex = 6;
      this.lblResultText1.Text = "Save All";
      // 
      // cmbScrollerText
      // 
      this.cmbScrollerText.FormattingEnabled = true;
      this.cmbScrollerText.Items.AddRange(new object[] {
            "",
            "",
            "",
            "Thanks to...",
            "DMA for the original game",
            "EricLang & ccexplore for Lemmix",
            "namida & Nepster for NeoLemmix",
            "Alex A. Denisov for Graphics32",
            "Erik Turner for ZLibEx",
            "Un4seen Development for Bass.dll",
            "The Lemmings Forums community at",
            "http://www.lemmingsforums.net",
            "Volker Oth and Mindless for",
            "sharing sourcecode and information",
            "What are you waiting for?",
            "Go play the game already!"});
      this.cmbScrollerText.Location = new System.Drawing.Point(110, 52);
      this.cmbScrollerText.MaxDropDownItems = 16;
      this.cmbScrollerText.Name = "cmbScrollerText";
      this.cmbScrollerText.Size = new System.Drawing.Size(250, 21);
      this.cmbScrollerText.TabIndex = 5;
      this.cmbScrollerText.Leave += new System.EventHandler(this.cmbScrollerText_Leave);
      // 
      // lblScrollerText
      // 
      this.lblScrollerText.AutoSize = true;
      this.lblScrollerText.Location = new System.Drawing.Point(8, 55);
      this.lblScrollerText.Name = "lblScrollerText";
      this.lblScrollerText.Size = new System.Drawing.Size(66, 13);
      this.lblScrollerText.TabIndex = 4;
      this.lblScrollerText.Text = "Scroller Text";
      // 
      // txtTitle2
      // 
      this.txtTitle2.Location = new System.Drawing.Point(110, 28);
      this.txtTitle2.MaxLength = 32;
      this.txtTitle2.Name = "txtTitle2";
      this.txtTitle2.Size = new System.Drawing.Size(250, 20);
      this.txtTitle2.TabIndex = 3;
      this.txtTitle2.Text = "by Unknown Author";
      this.txtTitle2.Leave += new System.EventHandler(this.txtTitle2_Leave);
      // 
      // txtTitle
      // 
      this.txtTitle.Location = new System.Drawing.Point(110, 6);
      this.txtTitle.MaxLength = 32;
      this.txtTitle.Name = "txtTitle";
      this.txtTitle.Size = new System.Drawing.Size(250, 20);
      this.txtTitle.TabIndex = 2;
      this.txtTitle.Text = "Unknown Pack";
      this.txtTitle.Leave += new System.EventHandler(this.txtTitle_Leave);
      // 
      // lblTitle2
      // 
      this.lblTitle2.AutoSize = true;
      this.lblTitle2.Location = new System.Drawing.Point(8, 31);
      this.lblTitle2.Name = "lblTitle2";
      this.lblTitle2.Size = new System.Drawing.Size(94, 13);
      this.lblTitle2.TabIndex = 1;
      this.lblTitle2.Text = "Title (second Line)";
      // 
      // lblTitle
      // 
      this.lblTitle.AutoSize = true;
      this.lblTitle.Location = new System.Drawing.Point(7, 9);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(27, 13);
      this.lblTitle.TabIndex = 0;
      this.lblTitle.Text = "Title";
      // 
      // PageLevels
      // 
      this.PageLevels.Controls.Add(this.butLevelReplace);
      this.PageLevels.Controls.Add(this.butLevelRankDown);
      this.PageLevels.Controls.Add(this.butLevelRankUp);
      this.PageLevels.Controls.Add(this.listTalismans);
      this.PageLevels.Controls.Add(this.listRanks);
      this.PageLevels.Controls.Add(this.txtRankName);
      this.PageLevels.Controls.Add(this.lblRankName);
      this.PageLevels.Controls.Add(this.listLevels);
      this.PageLevels.Controls.Add(this.butTalismanChange);
      this.PageLevels.Controls.Add(this.butTalismanDelete);
      this.PageLevels.Controls.Add(this.butTalismanAdd);
      this.PageLevels.Controls.Add(this.butPostview);
      this.PageLevels.Controls.Add(this.butPreview);
      this.PageLevels.Controls.Add(this.butLevelDown);
      this.PageLevels.Controls.Add(this.butLevelUp);
      this.PageLevels.Controls.Add(this.butLevelDelete);
      this.PageLevels.Controls.Add(this.butLevelAdd);
      this.PageLevels.Controls.Add(this.butRankDown);
      this.PageLevels.Controls.Add(this.butRankUp);
      this.PageLevels.Controls.Add(this.butRankDelete);
      this.PageLevels.Controls.Add(this.butRankAdd);
      this.PageLevels.Location = new System.Drawing.Point(4, 22);
      this.PageLevels.Name = "PageLevels";
      this.PageLevels.Padding = new System.Windows.Forms.Padding(3);
      this.PageLevels.Size = new System.Drawing.Size(366, 348);
      this.PageLevels.TabIndex = 1;
      this.PageLevels.Text = "Levels";
      this.PageLevels.UseVisualStyleBackColor = true;
      // 
      // butLevelReplace
      // 
      this.butLevelReplace.Location = new System.Drawing.Point(154, 135);
      this.butLevelReplace.Name = "butLevelReplace";
      this.butLevelReplace.Size = new System.Drawing.Size(100, 29);
      this.butLevelReplace.TabIndex = 9;
      this.butLevelReplace.Text = "Replace Level";
      this.butLevelReplace.UseVisualStyleBackColor = true;
      this.butLevelReplace.Click += new System.EventHandler(this.butLevelReplace_Click);
      // 
      // butLevelRankDown
      // 
      this.butLevelRankDown.Location = new System.Drawing.Point(260, 205);
      this.butLevelRankDown.Name = "butLevelRankDown";
      this.butLevelRankDown.Size = new System.Drawing.Size(100, 29);
      this.butLevelRankDown.TabIndex = 13;
      this.butLevelRankDown.Text = "Down One Rank";
      this.butLevelRankDown.UseVisualStyleBackColor = true;
      this.butLevelRankDown.Click += new System.EventHandler(this.butLevelRankDown_Click);
      // 
      // butLevelRankUp
      // 
      this.butLevelRankUp.Location = new System.Drawing.Point(154, 205);
      this.butLevelRankUp.Name = "butLevelRankUp";
      this.butLevelRankUp.Size = new System.Drawing.Size(100, 29);
      this.butLevelRankUp.TabIndex = 12;
      this.butLevelRankUp.Text = "Up One Rank";
      this.butLevelRankUp.UseVisualStyleBackColor = true;
      this.butLevelRankUp.Click += new System.EventHandler(this.butLevelRankUp_Click);
      // 
      // listTalismans
      // 
      this.listTalismans.FormattingEnabled = true;
      this.listTalismans.Location = new System.Drawing.Point(3, 275);
      this.listTalismans.Name = "listTalismans";
      this.listTalismans.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.listTalismans.Size = new System.Drawing.Size(140, 69);
      this.listTalismans.TabIndex = 16;
      this.listTalismans.SelectedIndexChanged += new System.EventHandler(this.listTalismans_SelectedIndexChanged);
      // 
      // listRanks
      // 
      this.listRanks.FormattingEnabled = true;
      this.listRanks.Items.AddRange(new object[] {
            "No ranks..."});
      this.listRanks.Location = new System.Drawing.Point(3, 3);
      this.listRanks.Name = "listRanks";
      this.listRanks.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.listRanks.Size = new System.Drawing.Size(140, 95);
      this.listRanks.TabIndex = 0;
      this.listRanks.SelectedIndexChanged += new System.EventHandler(this.listRanks_SelectedIndexChanged);
      // 
      // txtRankName
      // 
      this.txtRankName.Location = new System.Drawing.Point(227, 75);
      this.txtRankName.Name = "txtRankName";
      this.txtRankName.Size = new System.Drawing.Size(134, 20);
      this.txtRankName.TabIndex = 5;
      this.txtRankName.Leave += new System.EventHandler(this.txtRankName_Leave);
      // 
      // lblRankName
      // 
      this.lblRankName.AutoSize = true;
      this.lblRankName.Location = new System.Drawing.Point(157, 78);
      this.lblRankName.Name = "lblRankName";
      this.lblRankName.Size = new System.Drawing.Size(62, 13);
      this.lblRankName.TabIndex = 30;
      this.lblRankName.Text = "Rank name";
      // 
      // listLevels
      // 
      this.listLevels.FormattingEnabled = true;
      this.listLevels.Items.AddRange(new object[] {
            "No levels..."});
      this.listLevels.Location = new System.Drawing.Point(3, 100);
      this.listLevels.Name = "listLevels";
      this.listLevels.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.listLevels.Size = new System.Drawing.Size(140, 173);
      this.listLevels.TabIndex = 6;
      this.listLevels.SelectedIndexChanged += new System.EventHandler(this.listLevels_SelectedIndexChanged);
      // 
      // butTalismanChange
      // 
      this.butTalismanChange.Location = new System.Drawing.Point(154, 310);
      this.butTalismanChange.Name = "butTalismanChange";
      this.butTalismanChange.Size = new System.Drawing.Size(100, 29);
      this.butTalismanChange.TabIndex = 19;
      this.butTalismanChange.Text = "Change Talisman";
      this.butTalismanChange.UseVisualStyleBackColor = true;
      this.butTalismanChange.Click += new System.EventHandler(this.butTalismanChange_Click);
      // 
      // butTalismanDelete
      // 
      this.butTalismanDelete.Location = new System.Drawing.Point(260, 275);
      this.butTalismanDelete.Name = "butTalismanDelete";
      this.butTalismanDelete.Size = new System.Drawing.Size(100, 29);
      this.butTalismanDelete.TabIndex = 18;
      this.butTalismanDelete.Text = "Delete Talisman";
      this.butTalismanDelete.UseVisualStyleBackColor = true;
      this.butTalismanDelete.Click += new System.EventHandler(this.butTalismanDelete_Click);
      // 
      // butTalismanAdd
      // 
      this.butTalismanAdd.Location = new System.Drawing.Point(154, 275);
      this.butTalismanAdd.Name = "butTalismanAdd";
      this.butTalismanAdd.Size = new System.Drawing.Size(100, 29);
      this.butTalismanAdd.TabIndex = 17;
      this.butTalismanAdd.Text = "Add Talisman";
      this.butTalismanAdd.UseVisualStyleBackColor = true;
      this.butTalismanAdd.Click += new System.EventHandler(this.butTalismanAdd_Click);
      // 
      // butPostview
      // 
      this.butPostview.Location = new System.Drawing.Point(261, 240);
      this.butPostview.Name = "butPostview";
      this.butPostview.Size = new System.Drawing.Size(100, 29);
      this.butPostview.TabIndex = 15;
      this.butPostview.Text = "Postview Text";
      this.butPostview.UseVisualStyleBackColor = true;
      this.butPostview.Click += new System.EventHandler(this.butPostview_Click);
      // 
      // butPreview
      // 
      this.butPreview.Location = new System.Drawing.Point(154, 240);
      this.butPreview.Name = "butPreview";
      this.butPreview.Size = new System.Drawing.Size(100, 29);
      this.butPreview.TabIndex = 14;
      this.butPreview.Text = "Preview Text";
      this.butPreview.UseVisualStyleBackColor = true;
      this.butPreview.Click += new System.EventHandler(this.butPreview_Click);
      // 
      // butLevelDown
      // 
      this.butLevelDown.Location = new System.Drawing.Point(260, 170);
      this.butLevelDown.Name = "butLevelDown";
      this.butLevelDown.Size = new System.Drawing.Size(100, 29);
      this.butLevelDown.TabIndex = 11;
      this.butLevelDown.Text = "Move Down";
      this.butLevelDown.UseVisualStyleBackColor = true;
      this.butLevelDown.Click += new System.EventHandler(this.butLevelDown_Click);
      // 
      // butLevelUp
      // 
      this.butLevelUp.Location = new System.Drawing.Point(154, 170);
      this.butLevelUp.Name = "butLevelUp";
      this.butLevelUp.Size = new System.Drawing.Size(100, 29);
      this.butLevelUp.TabIndex = 10;
      this.butLevelUp.Text = "Move Up";
      this.butLevelUp.UseVisualStyleBackColor = true;
      this.butLevelUp.Click += new System.EventHandler(this.butLevelUp_Click);
      // 
      // butLevelDelete
      // 
      this.butLevelDelete.Location = new System.Drawing.Point(260, 100);
      this.butLevelDelete.Name = "butLevelDelete";
      this.butLevelDelete.Size = new System.Drawing.Size(100, 29);
      this.butLevelDelete.TabIndex = 8;
      this.butLevelDelete.Text = "Delete Level";
      this.butLevelDelete.UseVisualStyleBackColor = true;
      this.butLevelDelete.Click += new System.EventHandler(this.butLevelDelete_Click);
      // 
      // butLevelAdd
      // 
      this.butLevelAdd.Location = new System.Drawing.Point(154, 100);
      this.butLevelAdd.Name = "butLevelAdd";
      this.butLevelAdd.Size = new System.Drawing.Size(100, 29);
      this.butLevelAdd.TabIndex = 7;
      this.butLevelAdd.Text = "Add Level";
      this.butLevelAdd.UseVisualStyleBackColor = true;
      this.butLevelAdd.Click += new System.EventHandler(this.butLevelAdd_Click);
      // 
      // butRankDown
      // 
      this.butRankDown.Location = new System.Drawing.Point(260, 38);
      this.butRankDown.Name = "butRankDown";
      this.butRankDown.Size = new System.Drawing.Size(100, 29);
      this.butRankDown.TabIndex = 4;
      this.butRankDown.Text = "Move Down";
      this.butRankDown.UseVisualStyleBackColor = true;
      this.butRankDown.Click += new System.EventHandler(this.butRankDown_Click);
      // 
      // butRankUp
      // 
      this.butRankUp.Location = new System.Drawing.Point(154, 38);
      this.butRankUp.Name = "butRankUp";
      this.butRankUp.Size = new System.Drawing.Size(100, 29);
      this.butRankUp.TabIndex = 3;
      this.butRankUp.Text = "Move Up";
      this.butRankUp.UseVisualStyleBackColor = true;
      this.butRankUp.Click += new System.EventHandler(this.butRankUp_Click);
      // 
      // butRankDelete
      // 
      this.butRankDelete.Location = new System.Drawing.Point(260, 3);
      this.butRankDelete.Name = "butRankDelete";
      this.butRankDelete.Size = new System.Drawing.Size(100, 29);
      this.butRankDelete.TabIndex = 2;
      this.butRankDelete.Text = "Delete Rank";
      this.butRankDelete.UseVisualStyleBackColor = true;
      this.butRankDelete.Click += new System.EventHandler(this.butDeleteRank_Click);
      // 
      // butRankAdd
      // 
      this.butRankAdd.Location = new System.Drawing.Point(154, 3);
      this.butRankAdd.Name = "butRankAdd";
      this.butRankAdd.Size = new System.Drawing.Size(100, 29);
      this.butRankAdd.TabIndex = 1;
      this.butRankAdd.Text = "Add Rank";
      this.butRankAdd.UseVisualStyleBackColor = true;
      this.butRankAdd.Click += new System.EventHandler(this.butAddRank_Click);
      // 
      // PageMusic
      // 
      this.PageMusic.Controls.Add(this.listMusic);
      this.PageMusic.Controls.Add(this.butMusicDelete);
      this.PageMusic.Controls.Add(this.butMusicAdd);
      this.PageMusic.Location = new System.Drawing.Point(4, 22);
      this.PageMusic.Name = "PageMusic";
      this.PageMusic.Padding = new System.Windows.Forms.Padding(3);
      this.PageMusic.Size = new System.Drawing.Size(366, 348);
      this.PageMusic.TabIndex = 2;
      this.PageMusic.Text = "Music";
      this.PageMusic.UseVisualStyleBackColor = true;
      // 
      // listMusic
      // 
      this.listMusic.FormattingEnabled = true;
      this.listMusic.Items.AddRange(new object[] {
            "No music..."});
      this.listMusic.Location = new System.Drawing.Point(4, 5);
      this.listMusic.Name = "listMusic";
      this.listMusic.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.listMusic.Size = new System.Drawing.Size(141, 329);
      this.listMusic.Sorted = true;
      this.listMusic.TabIndex = 13;
      this.listMusic.SelectedIndexChanged += new System.EventHandler(this.listMusic_SelectedIndexChanged);
      // 
      // butMusicDelete
      // 
      this.butMusicDelete.Location = new System.Drawing.Point(154, 41);
      this.butMusicDelete.Name = "butMusicDelete";
      this.butMusicDelete.Size = new System.Drawing.Size(100, 29);
      this.butMusicDelete.TabIndex = 15;
      this.butMusicDelete.Text = "Delete Music";
      this.butMusicDelete.UseVisualStyleBackColor = true;
      this.butMusicDelete.Click += new System.EventHandler(this.butMusicDelete_Click);
      // 
      // butMusicAdd
      // 
      this.butMusicAdd.Location = new System.Drawing.Point(154, 6);
      this.butMusicAdd.Name = "butMusicAdd";
      this.butMusicAdd.Size = new System.Drawing.Size(100, 29);
      this.butMusicAdd.TabIndex = 14;
      this.butMusicAdd.Text = "Add Music";
      this.butMusicAdd.UseVisualStyleBackColor = true;
      this.butMusicAdd.Click += new System.EventHandler(this.butMusicAdd_Click);
      // 
      // PageSprites
      // 
      this.PageSprites.Controls.Add(this.picBoxSprite);
      this.PageSprites.Controls.Add(this.butSpriteDelete);
      this.PageSprites.Controls.Add(this.butSpriteAdd);
      this.PageSprites.Controls.Add(this.cmbSprites);
      this.PageSprites.Location = new System.Drawing.Point(4, 22);
      this.PageSprites.Name = "PageSprites";
      this.PageSprites.Padding = new System.Windows.Forms.Padding(3);
      this.PageSprites.Size = new System.Drawing.Size(366, 348);
      this.PageSprites.TabIndex = 3;
      this.PageSprites.Text = "Sprites";
      this.PageSprites.UseVisualStyleBackColor = true;
      this.PageSprites.Enter += new System.EventHandler(this.PageSprites_Enter);
      // 
      // picBoxSprite
      // 
      this.picBoxSprite.BackColor = System.Drawing.SystemColors.ControlLightLight;
      this.picBoxSprite.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.picBoxSprite.Location = new System.Drawing.Point(8, 39);
      this.picBoxSprite.Name = "picBoxSprite";
      this.picBoxSprite.Size = new System.Drawing.Size(352, 188);
      this.picBoxSprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.picBoxSprite.TabIndex = 3;
      this.picBoxSprite.TabStop = false;
      // 
      // butSpriteDelete
      // 
      this.butSpriteDelete.Location = new System.Drawing.Point(271, 12);
      this.butSpriteDelete.Name = "butSpriteDelete";
      this.butSpriteDelete.Size = new System.Drawing.Size(89, 21);
      this.butSpriteDelete.TabIndex = 2;
      this.butSpriteDelete.Text = "Delete Sprite";
      this.butSpriteDelete.UseVisualStyleBackColor = true;
      this.butSpriteDelete.Click += new System.EventHandler(this.butSpriteDelete_Click);
      // 
      // butSpriteAdd
      // 
      this.butSpriteAdd.Location = new System.Drawing.Point(176, 12);
      this.butSpriteAdd.Name = "butSpriteAdd";
      this.butSpriteAdd.Size = new System.Drawing.Size(89, 21);
      this.butSpriteAdd.TabIndex = 1;
      this.butSpriteAdd.Text = "Add Sprite";
      this.butSpriteAdd.UseVisualStyleBackColor = true;
      this.butSpriteAdd.Click += new System.EventHandler(this.butSpriteAdd_Click);
      // 
      // cmbSprites
      // 
      this.cmbSprites.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbSprites.FormattingEnabled = true;
      this.cmbSprites.Location = new System.Drawing.Point(8, 12);
      this.cmbSprites.Name = "cmbSprites";
      this.cmbSprites.Size = new System.Drawing.Size(162, 21);
      this.cmbSprites.TabIndex = 0;
      this.cmbSprites.SelectedValueChanged += new System.EventHandler(this.cmbSprites_SelectedValueChanged);
      // 
      // FormMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(377, 403);
      this.Controls.Add(this.tabPack);
      this.Controls.Add(this.menuStrip1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.menuStrip1;
      this.MaximizeBox = false;
      this.Name = "FormMain";
      this.Text = "Level Pack Toolkit";
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.tabPack.ResumeLayout(false);
      this.PageGeneral.ResumeLayout(false);
      this.PageGeneral.PerformLayout();
      this.PageLevels.ResumeLayout(false);
      this.PageLevels.PerformLayout();
      this.PageMusic.ResumeLayout(false);
      this.PageSprites.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.picBoxSprite)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.TabControl tabPack;
        private System.Windows.Forms.TabPage PageGeneral;
        private System.Windows.Forms.TabPage PageLevels;
        private System.Windows.Forms.TabPage PageMusic;
        private System.Windows.Forms.TabPage PageSprites;
        private MultilineTextBox cmbScrollerText;
        private System.Windows.Forms.Label lblScrollerText;
        private System.Windows.Forms.TextBox txtTitle2;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label lblTitle2;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblResultTexts;
        private MultilineTextBox cmbResultText1;
        private System.Windows.Forms.Label lblResultText1;
        private MultilineTextBox cmbResultText9;
        private System.Windows.Forms.Label lblResultText9;
        private MultilineTextBox cmbResultText8;
        private System.Windows.Forms.Label lblResultText8;
        private MultilineTextBox cmbResultText7;
        private System.Windows.Forms.Label lblResultText7;
        private MultilineTextBox cmbResultText6;
        private System.Windows.Forms.Label lblResultText6;
        private MultilineTextBox cmbResultText5;
        private System.Windows.Forms.Label lblResultText5;
        private MultilineTextBox cmbResultText4;
        private System.Windows.Forms.Label lblResultText4;
        private MultilineTextBox cmbResultText3;
        private System.Windows.Forms.Label lblResultText3;
        private MultilineTextBox cmbResultText2;
        private System.Windows.Forms.Label lblResultText2;
        private System.Windows.Forms.Button butPostview;
        private System.Windows.Forms.Button butPreview;
        private System.Windows.Forms.Button butLevelDown;
        private System.Windows.Forms.Button butLevelUp;
        private System.Windows.Forms.Button butLevelDelete;
        private System.Windows.Forms.Button butLevelAdd;
        private System.Windows.Forms.Button butRankDown;
        private System.Windows.Forms.Button butRankUp;
        private System.Windows.Forms.Button butRankDelete;
        private System.Windows.Forms.Button butRankAdd;
        private System.Windows.Forms.Button butTalismanChange;
        private System.Windows.Forms.Button butTalismanDelete;
        private System.Windows.Forms.Button butTalismanAdd;
        private System.Windows.Forms.Button butMusicDelete;
        private System.Windows.Forms.Button butMusicAdd;
        private System.Windows.Forms.PictureBox picBoxSprite;
        private System.Windows.Forms.Button butSpriteDelete;
        private System.Windows.Forms.Button butSpriteAdd;
        private System.Windows.Forms.ComboBox cmbSprites;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ListBox listMusic;
        private System.Windows.Forms.ListBox listLevels;
        private System.Windows.Forms.ListBox listRanks;
        private System.Windows.Forms.TextBox txtRankName;
        private System.Windows.Forms.Label lblRankName;
        private System.Windows.Forms.ListBox listTalismans;
    private System.Windows.Forms.Button butLevelRankDown;
    private System.Windows.Forms.Button butLevelRankUp;
    private System.Windows.Forms.Button butLevelReplace;
  }
}

