﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PackToolkit
{
    class C
    {
        public static string Version
        {
            get
            {
                var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                return version.Major.ToString() + "." + version.Minor.ToString();
            }
        }

        public static string AppPath => System.Windows.Forms.Application.StartupPath + DirSep;
        public static string AppPathLevels => AppPath + "levels" + DirSep;

        public static char DirSep => System.IO.Path.DirectorySeparatorChar;
        public static string NewLine => Environment.NewLine;

        public enum ResultTextType { All, Great, Good, Exact, OneShort, NearPass, Bad, VeryBad, None }
        public static Array ResultTextTypeArray => Enum.GetValues(typeof(ResultTextType));
        public static IEnumerable<ResultTextType> ResultTextTypeList => Enum.GetValues(typeof(ResultTextType)).Cast<ResultTextType>().Reverse();

        public enum Sprite
        {
            TitleLogo, MenuBackground,
            SignPlay, SignChooseLevel, SignRank, SignOptions, SignTalisman, SignQuit,
            SkillPanel, EmptySkillSlot, SkillCount, SkillCountErase,
            Minimap,
            MenuFont, ScrollerLems, ScrollerSegment
        }
        public static Array SpriteArray => Enum.GetValues(typeof(C.Sprite));

        public static readonly string TitleDefault = "Unknown Pack";
        public static readonly string Title2Default = "by Unknown Author";

        public static readonly string[] ScrollerTextDefault = new string[16]
        { "", "",
          "", "Thanks to...",
          "DMA for the original game", "EricLang & ccexplore for Lemmix",
          "namida & Nepster for NeoLemmix", "Alex A. Denisov for Graphics32",
          "base2 technologies for ZLibEx", "Un4seen Development for Bass.dll",
          "The Lemmings Forums community at", "http://www.lemmingsforums.net",
          "Volker Oth and Mindless for", "sharing sourcecode and information",
          "What are you waiting for?", "Go play the game already!"};

        public static readonly Dictionary<ResultTextType, string[]> ResultTextDefault = new Dictionary<ResultTextType, string[]>
        {
            { ResultTextType.All, new string[2] { "Superb! You rescued every lemming", "on that one. Can you do it again?" } },
            { ResultTextType.Great, new string[2] { "You totally stormed that level!", "Let's see if you can do it again..." } },
            { ResultTextType.Good, new string[2] { "That level seemed no problem to you", "on that attempt. Onto the next...." } },
            { ResultTextType.Exact, new string[2] { "RIGHT ON. You can't get much closer", "than that. Let's try the next..." } },
            { ResultTextType.OneShort, new string[2] { "OH NO, So near and yet so far...", "Maybe this time....." } },
            { ResultTextType.NearPass, new string[2] { "You got pretty close that time.", "Now try again for a few lemmings extra." } },
            { ResultTextType.Bad, new string[2] { "A little more practice on the level", "is definitely recommended." } },
            { ResultTextType.VeryBad, new string[2] { "Better rethink your strategy before", "you try this level again!" } },
            { ResultTextType.None, new string[2] { "ROCK BOTTOM! I hope for your sake", "that you nuked that level." } }
        };

        public static readonly Dictionary<Sprite, string> SpriteName = new Dictionary<Sprite, string>()
        {
            { Sprite.TitleLogo, "Title Logo"},
            { Sprite.MenuBackground, "Menu Background"},
            { Sprite.SignPlay, "Sign 'Play Level'"},
            { Sprite.SignChooseLevel, "Sign 'Select Level'"},
            { Sprite.SignRank, "Sign 'Choose Rank'"},
            { Sprite.SignOptions, "Sign 'Options'"},
            { Sprite.SignTalisman, "Sign 'Talismans'"},
            { Sprite.SignQuit, "Sign 'Quit'"},
            { Sprite.SkillPanel, "Skill Panel"},
            //{ Sprite.SkillPanelMask, "Skill Panel Mask"},
            { Sprite.EmptySkillSlot, "Empty Skill Slot"},
            //{ Sprite.EmptySkillSlotMask, "Empty Skill Slot Mask"},
            { Sprite.SkillCount, "Skill Count Digits"},
            //{ Sprite.SkillCountMask, "Skill Count Mask"},
            { Sprite.SkillCountErase, "Skill Count Erase"},
            { Sprite.Minimap, "Minimap Area"},
            //{ Sprite.MinimapMask, "Minimap Area Mask"},
            { Sprite.MenuFont, "Menu Font"},
            { Sprite.ScrollerLems, "Scroller Lemmings"},
            { Sprite.ScrollerSegment, "Scroller Segment"},
        };
        public static Sprite GetSpriteFromKey(string keyText)
        {
            return C.SpriteName.First(pair => pair.Value.Equals(keyText)).Key;
        }

        private static readonly Dictionary<Sprite, string> DefaultSpritePath = new Dictionary<Sprite, string>()
        {
            { Sprite.TitleLogo, "gfx" + DirSep +"menu" + DirSep + "logo.png"},
            { Sprite.MenuBackground, "gfx" + DirSep +"menu" + DirSep + "background.png"},
            { Sprite.SignPlay, "gfx" + DirSep +"menu" + DirSep + "sign_play.png"},
            { Sprite.SignChooseLevel, "gfx" + DirSep +"menu" + DirSep + "sign_code.png"},
            { Sprite.SignRank, "gfx" + DirSep +"menu" + DirSep + "sign_rank.png"},
            { Sprite.SignOptions, "gfx" + DirSep +"menu" + DirSep + "sign_config.png"},
            { Sprite.SignTalisman, "gfx" + DirSep +"menu" + DirSep + "sign_talisman.png"},
            { Sprite.SignQuit, "gfx" + DirSep + "menu" + DirSep + "sign_quit.png"},
            { Sprite.SkillPanel, "styles" + DirSep + "default" + DirSep + "panel" + DirSep + "skill_panels.png"},
            //{ Sprite.SkillPanelMask, ""},
            { Sprite.EmptySkillSlot, "styles" + DirSep + "default" + DirSep + "panel" + DirSep + "empty_slot.png"},
            //{ Sprite.EmptySkillSlotMask, ""},
            { Sprite.SkillCount, "styles" + DirSep + "default" + DirSep + "panel" + DirSep + "skill_count_digits.png"},
            //{ Sprite.SkillCountMask, ""},
            { Sprite.SkillCountErase, "styles" + DirSep + "default" + DirSep + "panel" + DirSep + "skill_count_erase.png"},
            { Sprite.Minimap, "styles" + DirSep + "default" + DirSep + "panel" + DirSep + "minimap_region.png"},
            //{ Sprite.MinimapMask, ""},
            { Sprite.MenuFont, "gfx" + DirSep + "menu" + DirSep + "menu_font.png"},
            { Sprite.ScrollerLems, "gfx" + DirSep + "menu" + DirSep + "scroller_lemmings.png"},
            { Sprite.ScrollerSegment, "gfx" + DirSep + "menu" + DirSep + "scroller_segment.png"},
        };
        public static string GetDefaultSpritePath(Sprite sprite)
        {
            return AppPath + DefaultSpritePath[sprite];
        }

        public static readonly Dictionary<Sprite, string> PackSpritePath = new Dictionary<Sprite, string>()
        {
            { Sprite.TitleLogo, "logo.png"},
            { Sprite.MenuBackground, "background.png"},
            { Sprite.SignPlay, "sign_play.png"},
            { Sprite.SignChooseLevel, "sign_code.png"},
            { Sprite.SignRank, "sign_rank.png"},
            { Sprite.SignOptions, "sign_config.png"},
            { Sprite.SignTalisman, "sign_talisman.png"},
            { Sprite.SignQuit, "sign_quit.png"},
            { Sprite.SkillPanel, "skill_panels.png"},
            { Sprite.EmptySkillSlot, "empty_slot.png"},
            { Sprite.SkillCount, "skill_count_digits.png"},
            { Sprite.SkillCountErase, "skill_count_erase.png"},
            { Sprite.Minimap, "minimap_region.png"},
            { Sprite.MenuFont, "menu_font.png"},
            { Sprite.ScrollerLems, "scroller_lemmings.png"},
            { Sprite.ScrollerSegment, "scroller_segment.png"}
        };


        public static string DefaultRankSprite => C.AppPath + "gfx" + C.DirSep + "menu" + C.DirSep + "rank_graphic.png";


        public enum TalismanType { Bronze, Silver, Gold }
        public enum TalismanReq
        {
            SaveReq, TimeLimit, SkillTotal,
            SkillWalker, SkillClimber, SkillSwimmer, SkillFloater, SkillGlider, SkillShimmier,
            SkillDisarmer, SkillBomber, SkillStoner, SkillBlocker,
            SkillBuilder, SkillPlatformer, SkillStacker,
            SkillBasher, SkillMiner, SkillDigger, SkillFencer, SkillCloner,
            SkillEachLimit, UseOnlySkill
            //, OnlyOneWorker, RRMin, RRMax
        }
        public static Array TalismanReqArray => Enum.GetValues(typeof(C.TalismanReq));

        public static readonly List<string> TalismanSkills = new List<string>()
        {
            "Walker", "Climber", "Swimmer", "Floater", "Glider", "Shimmier",
            "Disarmer", "Bomber", "Stoner", "Blocker",
            "Builder", "Platformer", "Stacker",
            "Basher", "Miner", "Digger", "Fencer", "Cloner"
        };

        public static readonly Dictionary<TalismanReq, string> TalismanReqText = new Dictionary<TalismanReq, string>()
        {
            { TalismanReq.SaveReq, "Save Requirement" },
            { TalismanReq.TimeLimit, "Time Limit" },
            { TalismanReq.SkillTotal, "Limit Total Skills" },
            { TalismanReq.SkillWalker, "Limit Walkers" },
            { TalismanReq.SkillClimber, "Limit Climbers" },
            { TalismanReq.SkillSwimmer, "Limit Swimmers" },
            { TalismanReq.SkillFloater, "Limit Floaters" },
            { TalismanReq.SkillGlider, "Limit Gliders" },
            { TalismanReq.SkillShimmier, "Limit Shimmiers" },
            { TalismanReq.SkillDisarmer, "Limit Disarmers" },
            { TalismanReq.SkillBomber, "Limit Bombers" },
            { TalismanReq.SkillStoner, "Limit Stoners" },
            { TalismanReq.SkillBlocker, "Limit Blockers" },
            { TalismanReq.SkillBuilder, "Limit Builders" },
            { TalismanReq.SkillPlatformer, "Limit Platformers" },
            { TalismanReq.SkillStacker, "Limit Stackers" },
            { TalismanReq.SkillBasher, "Limit Bashers" },
            { TalismanReq.SkillMiner, "Limit Miners" },
            { TalismanReq.SkillDigger, "Limit Diggers" },
            { TalismanReq.SkillFencer, "Limit Fencers" },
            { TalismanReq.SkillCloner, "Limit Cloners" },
            { TalismanReq.SkillEachLimit, "Limit All Skills" },
            { TalismanReq.UseOnlySkill, "Using only the Skill" }
            //{ TalismanReq.OnlyOneWorker, "One Worker Lem" },
            //{ TalismanReq.RRMin, "RR Minimum" },
            //{ TalismanReq.RRMax, "RR Maximum" }
        };

        public static readonly Dictionary<TalismanReq, string> TalismanKeys = new Dictionary<TalismanReq, string>()
        {
            { TalismanReq.SaveReq, "SAVE" }, { TalismanReq.TimeLimit, "TIME_LIMIT" },
            { TalismanReq.SkillTotal, "SKILL_LIMIT" }, { TalismanReq.SkillWalker, "WALKER_LIMIT" },
            { TalismanReq.SkillClimber, "CLIMBER_LIMIT"}, { TalismanReq.SkillSwimmer, "SWIMMER_LIMIT"},
            { TalismanReq.SkillFloater, "FLOATER_LIMIT" }, { TalismanReq.SkillGlider, "GLIDER_LIMIT" },
            { TalismanReq.SkillShimmier, "SHIMMIER_LIMIT" },
            { TalismanReq.SkillDisarmer, "DISARMER_LIMIT" }, { TalismanReq.SkillBomber, "BOMBER_LIMIT" },
            { TalismanReq.SkillStoner, "STONER_LIMIT" }, { TalismanReq.SkillBlocker, "BLOCKER_LIMIT"},
            { TalismanReq.SkillBuilder, "BUILDER_LIMIT" }, { TalismanReq.SkillPlatformer, "PLATFORMER_LIMIT" },
            { TalismanReq.SkillStacker, "STACKER_LIMIT" }, { TalismanReq.SkillBasher, "BASHER_LIMIT" },
            { TalismanReq.SkillMiner, "MINER_LIMIT" }, { TalismanReq.SkillDigger, "DIGGER_LIMIT" },
            { TalismanReq.SkillFencer, "FENCER_LIMIT" }, { TalismanReq.SkillCloner, "CLONER_LIMIT" },
            { TalismanReq.SkillEachLimit, "SKILL_EACH_LIMIT" }, { TalismanReq.UseOnlySkill, "USE_ONLY_SKILL" }
        };

        public static readonly List<string> VersionList = new List<string>
        {
            "Version " + Version,
            "   by Stephan Neupert (Nepster)",
            "",
            "Thanks to...",
            "  DMA for creating the original Lemmings games.",
            "  Namida Verasche for the NeoLemmix player.",
            "  The LemmingsForums at http://www.lemmingsforums.net.",
            "",
            "This application and all its source code is licensed under",
            "   CC BY-NC 4.0."
        };
    }
}
