﻿using System;
using System.Collections.Generic;

namespace PackToolkit
{
  class Talisman
  {
    public Talisman(Pack pack, Level level)
    {
      curPack = pack;
      curLevel = level;
      AwardType = C.TalismanType.Bronze;
      Requirements = new Dictionary<C.TalismanReq, int>();
      Title = string.Empty;
      ID = -1;
    }

    Pack curPack;
    Level curLevel;
    public C.TalismanType AwardType;
    public Dictionary<C.TalismanReq, int> Requirements;
    public string Title;
    public int ID;

    /// <summary>
    /// Gets the text of the requirement to be displayed on the form.
    /// </summary>
    /// <param name="requirement"></param>
    /// <returns></returns>
    public string GetRequirementText(C.TalismanReq requirement)
    {
      if (!Requirements.ContainsKey(requirement)) return string.Empty;

      if (requirement == C.TalismanReq.TimeLimit)
      {
        int min = Requirements[requirement] / 17 / 60;
        int sec = (Requirements[requirement] / 17) % 60;
        return C.TalismanReqText[requirement] + ": " + min.ToString() + ":" + sec.ToString();
      }
      else if (requirement == C.TalismanReq.UseOnlySkill)
      {
        return C.TalismanReqText[requirement] + ": " + C.TalismanSkills[Requirements[requirement]];
      }
      else
      {
        return C.TalismanReqText[requirement] + ": " + Requirements[requirement].ToString();
      }
    }

  }
}
