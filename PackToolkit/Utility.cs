﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace PackToolkit
{
  public static class Env
  {
#if DEBUG
    public static readonly bool Debugging = true;
#else
    public static readonly bool Debugging = false;
#endif
  }

  static class Utility
  {
    /// <summary>
    /// Checks if an object is contained in an array.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public static bool In<T>(this T obj, params T[] args)
    {
      return args.Contains(obj);
    }

    /// <summary>
    /// Swaps two elements of given index in the list.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="myList"></param>
    /// <param name="index1"></param>
    /// <param name="index2"></param>
    public static void Swap<T>(this IList<T> myList, int index1, int index2)
    {
      T item = myList[index1];
      myList[index1] = myList[index2];
      myList[index2] = item;
    }

    /// <summary>
    /// Splits the string at all new-lines.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static List<string> SplitAtNewLine(this string text)
    {
      return text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None).ToList();
    }

    /// <summary>
    /// Parses a string value to an enum of given type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T ParseEnum<T>(string value)
    {
      return (T)Enum.Parse(typeof(T), value, true);
    }

    /// <summary>
    /// Opens the file browser and returns the selected files.
    /// </summary>
    /// <param name="Filter"></param>
    /// <param name="isMultiselect"></param>
    /// <returns></returns>
    public static string[] SelectFiles(string filter, bool isMultiselect, string defaultPath = null)
    {
      var openFileDialog = new OpenFileDialog();

      var startPath = !string.IsNullOrWhiteSpace(defaultPath) ? defaultPath
                : System.IO.Directory.Exists(C.AppPathLevels) ? C.AppPathLevels
                                                              : C.AppPath;
      openFileDialog.InitialDirectory = startPath;
      openFileDialog.Multiselect = isMultiselect;
      openFileDialog.Filter = filter;
      openFileDialog.RestoreDirectory = true;
      openFileDialog.CheckFileExists = true;

      string[] filePaths = new string[0];

      try
      {
        if (openFileDialog.ShowDialog() == DialogResult.OK)
        {
          filePaths = openFileDialog.FileNames;
        }
      }
      catch (Exception Ex)
      {
        MessageBox.Show("Error while showing the file browser." + C.NewLine + Ex.Message, "File browser error");
      }
      finally
      {
        openFileDialog?.Dispose();
      }

      return filePaths;
    }

    /// <summary>
    /// Get a folder path
    /// </summary>
    public static string SelectFolder(string defaultPath = null)
    {
      var startPath = !string.IsNullOrWhiteSpace(defaultPath) ? defaultPath
          : System.IO.Directory.Exists(C.AppPathLevels) ? C.AppPathLevels
                                                        : C.AppPath;

      var folderBrowser = new FolderBrowser.FolderBrowser2();
      folderBrowser.DirectoryPath = startPath;
      var result = folderBrowser.ShowDialog(null);
      return result == DialogResult.OK ? folderBrowser.DirectoryPath : null;
    }


    /// <summary>
    /// Loads a bitmap from a file and closes the file again.
    /// <para> It returns null if an exception occured. </para>
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static Bitmap CreateBitmapFromFile(string filePath)
    {
      Bitmap image;
      try
      {
        using (var tempImage = new Bitmap(filePath))
        {
          image = new Bitmap(tempImage);
        }
      }
      catch
      {
        image = null;
      }
      return image;
    }

    /// <summary>
    /// Replaces forbidden characters and whitespaces in to-be file-names.
    /// </summary>
    /// <param name="oldPath"></param>
    /// <returns></returns>
    public static string MakeFileNameCompliant(string oldPath)
    {
      return oldPath.Replace(' ', '_')
                    .Replace('<', '_')
                    .Replace('>', '_')
                    .Replace(':', '_')
                    .Replace('"', '_')
                    .Replace('/', '_')
                    .Replace('\\', '_')
                    .Replace('|', '_')
                    .Replace('?', '_')
                    .Replace('*', '_');
    }

    /// <summary>
    /// Handles a global unexpected exception and displays a warning message to the user.
    /// </summary>
    /// <param name="Ex"></param>
    public static void HandleGlobalException(Exception Ex)
    {
      try
      {
        Utility.LogException(Ex);
        string errorString = "Klopt niet: " + Ex.Message + C.NewLine + "Try to continue working on the level pack? Selecting 'no' will quit the pack editor.";
        var result = MessageBox.Show(errorString, "Error", MessageBoxButtons.YesNo);
        if (result == DialogResult.No) Application.Exit();
      }
      catch
      {
        Application.Exit();
      }
    }

    /// <summary>
    /// Logs an exception message to AppPath/ErrorLog.txt.
    /// </summary>
    /// <param name="ex"></param>
    public static void LogException(Exception ex)
    {
      string errorPath = C.AppPath + "ErrorLog.txt";
      System.IO.TextWriter textFile = new System.IO.StreamWriter(errorPath, true);
      textFile.WriteLine(ex.ToString());
      textFile.Close();
    }
  }

  /// <summary>
  /// A comparer for Windows file paths.
  /// </summary>
  class FilePathComparer : IEqualityComparer<string>
  {
    public FilePathComparer() { }

    public bool Equals(string file1, string file2)
    {
      return string.Equals(System.IO.Path.GetFullPath(file1), System.IO.Path.GetFullPath(file2), StringComparison.OrdinalIgnoreCase);
    }

    public int GetHashCode(string file)
    {
      return System.IO.Path.GetFullPath(file).ToUpper().GetHashCode();
    }
  }


}
