﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PackToolkit
{
  /// <summary>
  /// Here is all the code related to actions performed on the tab "Level"
  /// </summary>
  public partial class FormMain
  {
    private void tabPack_Enter(object sender, EventArgs e)
    {
      // Set all infos correctly
      WriteRankInfo();
      WriteLevelInfo();
      WriteTalismanInfo();

      SetButtonActive();
    }

    /// <summary>
    /// Gets the currently selected Rank.
    /// Return null if no rank exists, or the first one if no rank is selected.
    /// </summary>
    /// <returns></returns>
    private Rank GetCurRank()
    {
      if (curPack.Ranks.Count == 0)
      {
        return null;
      }
      else if (listRanks.SelectedIndices.Count == 0)
      {
        return curPack.Ranks[0];
      }
      else
      {
        return curPack.Ranks[listRanks.SelectedIndices[0]];
      }
    }

    /// <summary>
    /// Gets the currently selected Level in the current Rank.
    /// Return null if no rank or level exists, or the first one if no level is selected.
    /// </summary>
    /// <returns></returns>
    private Level GetCurLevel()
    {
      Rank curRank = GetCurRank();
      if (curRank == null) return null;

      if (curRank.Levels.Count == 0)
      {
        return null;
      }
      else if (listLevels.SelectedIndices.Count == 0)
      {
        return curRank.Levels[0];
      }
      else
      {
        return curRank.Levels[listLevels.SelectedIndices[0]];
      }
    }

    /// <summary>
    /// Gets the currently selected talisman of the current level, or returns null if none is selected.
    /// </summary>
    /// <returns></returns>
    private Talisman GetCurTalisman()
    {
      Level curLevel = GetCurLevel();
      if (curLevel == null) return null;

      if (curLevel.Talismans.Count == 0)
      {
        return null;
      }
      else if (listTalismans.SelectedIndices.Count == 0)
      {
        return curLevel.Talismans[0];
      }
      else
      {
        return curLevel.Talismans[listTalismans.SelectedIndices[0]];
      }
    }

    /// <summary>
    /// Adds a new rank with a generic name
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butAddRank_Click(object sender, EventArgs e)
    {
      if (curPack.Ranks.Count == 0) listRanks.Items.Clear();

      // Add rank to pack
      string newName = "Rank " + (curPack.Ranks.Count + 1).ToString();
      curPack.Ranks.Add(new Rank(newName));
      listRanks.Items.Add(newName);
      isSaved = false;

      SetButtonActive();
      WriteLevelInfo();
      WriteTalismanInfo();
    }

    /// <summary>
    /// Deletes all selected ranks.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butDeleteRank_Click(object sender, EventArgs e)
    {
      if (curPack.Ranks.Count == 0) return;

      // Remove ranks from pack
      int[] removeIndexes = listRanks.SelectedIndices.Cast<int>().ToArray();
      curPack.Ranks.RemoveAll(rank => removeIndexes.Contains(curPack.Ranks.IndexOf(rank)));

      // Remove ranks from listRanks
      for (int i = listRanks.SelectedItems.Count - 1; i >= 0; i--)
      {
        listRanks.Items.Remove(listRanks.SelectedItems[i]);
      }

      // If we deleted the last rank, add the default item.
      if (listRanks.Items.Count == 0)
      {
        listRanks.Items.Add("No ranks...");
      }

      isSaved = false;
      SetButtonActive();
      WriteLevelInfo();
      WriteTalismanInfo();
    }

    /// <summary>
    /// Moves all selected ranks up one position.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butRankUp_Click(object sender, EventArgs e)
    {
      MoveSelectionUp(listRanks, curPack.Ranks);
      isSaved = false;
    }

    /// <summary>
    /// Moves all selected ranks down one position.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butRankDown_Click(object sender, EventArgs e)
    {
      MoveSelectionDown(listRanks, curPack.Ranks);
      isSaved = false;
    }

    /// <summary>
    /// Update the rank name of the currently selected rank.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtRankName_Leave(object sender, EventArgs e)
    {
      if (listRanks.SelectedIndices.Count == 0) return;

      GetCurRank().Name = txtRankName.Text;
      listRanks.Items[listRanks.SelectedIndex] = txtRankName.Text;
      isSaved = false;
    }

    /// <summary>
    /// Update the levels and talismans according to selection
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listRanks_SelectedIndexChanged(object sender, EventArgs e)
    {
      txtRankName.Text = (listRanks.SelectedIndices.Count > 0) ? GetCurRank().Name : string.Empty;

      WriteLevelInfo();
      WriteTalismanInfo();
      SetButtonActive();
    }

    /// <summary>
    /// Update buttons if a new level is selected.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listLevels_SelectedIndexChanged(object sender, EventArgs e)
    {
      WriteTalismanInfo();
      SetButtonActive();
    }

    /// <summary>
    /// Adds new levels to the rank, that the user selects from a file browser.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butLevelAdd_Click(object sender, EventArgs e)
    {
      if (GetCurRank() == null) return;
      if (GetCurRank().Levels.Count == 0) listLevels.Items.Clear();

      // Open file browser
      string filter = "NeoLemmix level files (*.nxlv)|*.nxlv";
      string[] filePaths = Utility.SelectFiles(filter, true);

      // and add the selected levels to the rank.
      foreach (string filePath in filePaths)
      {
        Level newLevel = new Level(filePath, curPack);
        GetCurRank().Levels.Add(newLevel);
        listLevels.Items.Add(newLevel.Name);
      }

      // If we didn't add any level, and the rank is still empty, add the default one.
      if (listLevels.Items.Count == 0)
      {
        listLevels.Items.Add("No levels...");
      }

      isSaved = false;
      WriteTalismanInfo();
      SetButtonActive();
    }

    /// <summary>
    /// Removes all selected levels from the rank.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butLevelDelete_Click(object sender, EventArgs e)
    {
      Rank rank = GetCurRank();
      if (rank == null || rank.Levels.Count == 0) return;

      // Remove levels from rank
      int[] removeIndexes = listLevels.SelectedIndices.Cast<int>().ToArray();
      rank.Levels.RemoveAll(level => removeIndexes.Contains(rank.Levels.IndexOf(level)));

      // Remove levels from listLevels
      for (int i = listLevels.SelectedItems.Count - 1; i >= 0; i--)
      {
        listLevels.Items.Remove(listLevels.SelectedItems[i]);
      }

      // If we deleted the last level, add the default item.
      if (listLevels.Items.Count == 0)
      {
        listLevels.Items.Add("No levels...");
      }

      isSaved = false;
      WriteTalismanInfo();
      SetButtonActive();
    }

    /// <summary>
    /// Replaces a level with one that the user selects in the file browser
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butLevelReplace_Click(object sender, EventArgs e)
    {
      Rank rank = GetCurRank();
      var levelIndex = listLevels.SelectedIndex;
      if (rank == null || levelIndex < 0) return;

      // Open file browser
      string filter = "NeoLemmix level files (*.nxlv)|*.nxlv";
      string[] filePaths = Utility.SelectFiles(filter, false);
      if (filePaths == null || filePaths.Count() == 0) return;
      string filePath = filePaths[0];

      // Replace the levels in internal list
      Level newLevel = new Level(filePath, curPack);
      rank.Levels[levelIndex] = newLevel;

      // Update ListLevels
      listLevels.Items.RemoveAt(levelIndex);
      listLevels.Items.Insert(levelIndex, newLevel.Name);
      listLevels.SetSelected(levelIndex, true);

      isSaved = false;
      WriteTalismanInfo();
      SetButtonActive();
    }


    /// <summary>
    /// Moves all selected levels up one position.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butLevelUp_Click(object sender, EventArgs e)
    {
      Rank rank = GetCurRank();
      if (rank == null) return;

      MoveSelectionUp(listLevels, rank.Levels);
      isSaved = false;
    }

    /// <summary>
    /// Moves all selected ranks down one position.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butLevelDown_Click(object sender, EventArgs e)
    {
      Rank rank = GetCurRank();
      if (rank == null) return;

      MoveSelectionDown(listLevels, rank.Levels);
      isSaved = false;
    }

    /// <summary>
    /// Moves all selected levels up one rank
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butLevelRankUp_Click(object sender, EventArgs e)
    {
      MoveLevelSelectionInRanks(1);
    }

    /// <summary>
    /// Moves all selected levels down one rank
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butLevelRankDown_Click(object sender, EventArgs e)
    {
      MoveLevelSelectionInRanks(-1);
    }

    /// <summary>
    /// Moves all selected levels up or down in ranks, depending on the given delta
    /// </summary>
    /// <param name="rankDelta"></param>
    private void MoveLevelSelectionInRanks(int rankDelta)
    {
      Rank rank = GetCurRank();
      int newRankIndex = curPack.Ranks.IndexOf(rank) + rankDelta;
      if (rank == null || newRankIndex < 0 || newRankIndex >= curPack.Ranks.Count) return;

      List<Level> selection = listLevels.SelectedIndices.Cast<int>().Select(i => rank.Levels[i]).ToList();

      // Remove the levels from the current rank
      butLevelDelete_Click(null, null);
      // Add the levels
      curPack.Ranks[newRankIndex].Levels.AddRange(selection);
      // Move to next rank in the editor
      listRanks.SelectedItems.Clear();
      listRanks.SelectedIndex = newRankIndex;
      listRanks.SetSelected(newRankIndex, true);

      isSaved = false;
      WriteLevelInfo();
      
      // Select the previously selected levels again
      for (int index = 1; index <= selection.Count; index++)
      {
        listLevels.SetSelected(listLevels.Items.Count - index, true);
      }

      WriteTalismanInfo();
      SetButtonActive();
    }


    /// <summary>
    /// Opens the preview text editor for the currently selected level.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butPreview_Click(object sender, EventArgs e)
    {
      Level level = GetCurLevel();
      if (level == null) return;

      (new FormPrePostTest(level, true)).Show();
      isSaved = false;
    }

    /// <summary>
    /// Opens the postview text editor for the currently selected level.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butPostview_Click(object sender, EventArgs e)
    {
      Level level = GetCurLevel();
      if (level == null) return;

      (new FormPrePostTest(level, false)).Show();
      isSaved = false;
    }

    /// <summary>
    /// Update the tab, if a new talisman is selected.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listTalismans_SelectedIndexChanged(object sender, EventArgs e)
    {
      SetButtonActive();
    }

    /// <summary>
    /// Opens the talisman form to add a new talisman
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butTalismanAdd_Click(object sender, EventArgs e)
    {
      Level level = GetCurLevel();
      if (level == null) return;

      (new FormTalisman(curPack, level)).Show(this);

      isSaved = false;
      SetButtonActive();
    }

    /// <summary>
    /// Opens the talisman form to change the currently selected talisman
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butTalismanChange_Click(object sender, EventArgs e)
    {
      Level level = GetCurLevel();
      Talisman talisman = GetCurTalisman();
      if (talisman == null) return;

      (new FormTalisman(curPack, level, talisman)).Show(this);
      isSaved = false;
    }

    /// <summary>
    /// Deletes the selected talismans.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butTalismanDelete_Click(object sender, EventArgs e)
    {
      Level level = GetCurLevel();
      if (level == null || level.Talismans.Count == 0) return;

      // Remove levels from rank
      int[] removeIndexes = listTalismans.SelectedIndices.Cast<int>().ToArray();
      level.Talismans.RemoveAll(talis => removeIndexes.Contains(level.Talismans.IndexOf(talis)));

      // Remove levels from listLevels
      for (int i = listTalismans.SelectedItems.Count - 1; i >= 0; i--)
      {
        listTalismans.Items.Remove(listTalismans.SelectedItems[i]);
      }

      // If we deleted the last level, add the default item.
      if (listTalismans.Items.Count == 0)
      {
        listTalismans.Items.Add("No talismans...");
      }

      isSaved = false;
      SetButtonActive();
    }

    /// <summary>
    /// Moves all selected items up one position, both in the ListView and the correponding pack data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="listBox"></param>
    /// <param name="packList"></param>
    private void MoveSelectionUp<T>(ListBox listBox, List<T> packList)
    {
      if (listBox.SelectedIndices.Count == 0) return;

      List<int> selectionList = listBox.SelectedIndices.Cast<int>().ToList();
      selectionList.Sort();
      selectionList.RemoveAll(i => i == selectionList.IndexOf(i));

      // Swap items in pack
      foreach (int index in selectionList)
      {
        packList.Swap(index, index - 1);
      }

      // Swap items in list box
      foreach (int index in selectionList)
      {
        var listItem = listBox.Items[index];
        listBox.Items.RemoveAt(index);
        listBox.Items.Insert(index - 1, listItem);
        listBox.SetSelected(index - 1, true);
      }
    }

    /// <summary>
    /// Moves all selected items down one position, both in the ListView and the correponding pack data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="listBox"></param>
    /// <param name="packList"></param>
    private void MoveSelectionDown<T>(ListBox listBox, List<T> packList)
    {
      if (listBox.SelectedIndices.Count == 0) return;

      List<int> selectionList = listBox.SelectedIndices.Cast<int>().ToList();
      selectionList.Sort();
      selectionList.RemoveAll(i => packList.Count - i == selectionList.Count - selectionList.IndexOf(i));
      selectionList.Reverse();

      // Swap items in pack
      foreach (int index in selectionList)
      {
        packList.Swap(index, index + 1);
      }

      // Swap items in list box
      foreach (int index in selectionList)
      {
        var listItem = listBox.Items[index];
        listBox.Items.RemoveAt(index);
        listBox.Items.Insert(index + 1, listItem);
        listBox.SetSelected(index + 1, true);
      }
    }

    /// <summary>
    /// Sets the currently active buttons correctly.
    /// </summary>
    private void SetButtonActive()
    {
      butRankAdd.Enabled = true;
      bool isRankSelected = listRanks.SelectedIndices.Count > 0 && GetCurRank() != null;
      butRankDelete.Enabled = isRankSelected;
      butRankDown.Enabled = isRankSelected;
      butRankUp.Enabled = isRankSelected;
      txtRankName.Enabled = isRankSelected;

      butLevelAdd.Enabled = (GetCurRank() != null);
      bool isLevelSelected = listLevels.SelectedIndices.Count > 0 && GetCurLevel() != null;
      butLevelDelete.Enabled = isLevelSelected;
      butLevelReplace.Enabled = isLevelSelected && listLevels.SelectedIndices.Count == 1;
      butLevelDown.Enabled = isLevelSelected;
      butLevelUp.Enabled = isLevelSelected;
      butLevelRankUp.Enabled = isLevelSelected && GetCurRank() != curPack.Ranks.Last();
      butLevelRankDown.Enabled = isLevelSelected && GetCurRank() != curPack.Ranks.First();
      butPreview.Enabled = isLevelSelected;
      butPostview.Enabled = isLevelSelected;

      butTalismanAdd.Enabled = (GetCurLevel() != null);
      bool isTalismanSelected = listTalismans.SelectedIndices.Count > 0 && GetCurTalisman() != null;
      butTalismanDelete.Enabled = isTalismanSelected;
      butTalismanChange.Enabled = (GetCurTalisman() != null);
    }
  }
}
