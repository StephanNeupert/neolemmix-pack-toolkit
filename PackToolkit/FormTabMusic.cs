﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PackToolkit
{
    /// <summary>
    /// Here is all the code related to actions performed on the tab "Music"
    /// </summary>
    public partial class FormMain
    {
        /// <summary>
        /// Updates buttons on selecting music files.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listMusic_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetMusicButtonActive();
        }

        /// <summary>
        /// Adds new music files to the pack, that the user selects from a file browser.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butMusicAdd_Click(object sender, EventArgs e)
        {
            if (curPack.Musics.Count == 0) listMusic.Items.Clear();

            // Open file browser
            string filter = "Music files (*.ogg, *.mp3, *.mod, *.it)|*.ogg; *.mp3; *.mod; *.it; *.wav; *.xm";
            string[] filePaths = Utility.SelectFiles(filter, true);

            // and add the selected levels to the rank.
            foreach (string filePath in filePaths)
            {
                Music newMusic = new Music(filePath);
                if (!curPack.Musics.Exists(music => music.Equals(newMusic)))
                {
                    curPack.Musics.Add(newMusic);
                    listMusic.Items.Add(newMusic.Name);
                }
            }

            // If we didn't add any music file, and it is still empty, add the default one.
            if (listMusic.Items.Count == 0)
            {
                listMusic.Items.Add("No musics...");
            }

            SetMusicButtonActive();
            isSaved = false;
        }

        /// <summary>
        /// Removes all selected music files from the rank.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butMusicDelete_Click(object sender, EventArgs e)
        {
            if (curPack.Musics.Count == 0) return;

            // Ramove music files from pack
            List<string> removeMusicNames = listMusic.SelectedItems.Cast<string>().ToList();
            curPack.Musics.RemoveAll(music => removeMusicNames.Contains(music.Name));

            // Remove musics from listMusic
            for (int i = listMusic.SelectedItems.Count - 1; i >= 0; i--)
            {
                listMusic.Items.Remove(listMusic.SelectedItems[i]);
            }

            // If we deleted the last music file, add the default item.
            if (listMusic.Items.Count == 0)
            {
                listMusic.Items.Add("No musics...");
            }

            SetMusicButtonActive();
            isSaved = false;
        }

        /// <summary>
        /// Enables the delete button if appropriate.
        /// </summary>
        private void SetMusicButtonActive()
        {
            butMusicDelete.Enabled = curPack.Musics.Count > 0 && listMusic.SelectedIndices.Count > 0;
        }
    }
}
