﻿using System;
using System.Collections.Generic;

namespace PackToolkit
{
  class Pack
  {
    public Pack()
    {
      Title = C.TitleDefault;
      Title2 = C.Title2Default;

      ScrollerText = C.ScrollerTextDefault;
      ResultText = C.ResultTextDefault;

      Ranks = new List<Rank>() { new Rank("Rank 1") };
      Musics = new List<Music>();
      MusicRotation = new List<string>();
      Sprites = new Dictionary<C.Sprite, string>();
    }

    public string Title;
    public string Title2;

    public string[] ScrollerText;
    public Dictionary<C.ResultTextType, string[]> ResultText;

    public List<Rank> Ranks;
    public List<Music> Musics;
    public List<string> MusicRotation;
    public Dictionary<C.Sprite, string> Sprites;

    /// <summary>
    /// Gets the key for the sprite corresponding to the rank
    /// </summary>
    /// <param name="rank"></param>
    /// <returns></returns>
    public string GetSpriteName(Rank rank)
    {
      int rankIndex = Ranks.IndexOf(rank);
      return "Rank " + (rankIndex + 1).ToString() + ": '" + rank.Name + "'";
    }

    /// <summary>
    /// Gets a list of keys for the sprites for all rank images
    /// </summary>
    /// <returns></returns>
    public List<string> GetSpriteNames()
    {
      return Ranks.ConvertAll(rank => GetSpriteName(rank));
    }

    /// <summary>
    /// Sets all IDs of Talismans correctly, ascending within the pack
    /// </summary>
    public void SetTalismanIndex()
    {
      foreach (Rank rank in Ranks)
      {
        foreach (Level level in rank.Levels)
        {
          foreach (Talisman talisman in level.Talismans.FindAll(tal => tal.ID < 0))
          {
            var IDs = level.Talismans.ConvertAll(tal => tal.ID);
            int index = 0;
            while (IDs.Contains(index))
            {
              index++;
            }
            talisman.ID = index;
          }
        }
      }
    }




  }
}
